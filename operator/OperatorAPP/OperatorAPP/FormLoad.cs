﻿using SushiLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperatorAPP
{
    public partial class FormLoad : Form
    {
        public FormLoad()
        {
            Graphics dpiGraphics = Graphics.FromHwnd(IntPtr.Zero);
            this.AutoScaleDimensions = new SizeF(dpiGraphics.DpiX, dpiGraphics.DpiX);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            dpiGraphics.Dispose();
            InitializeComponent();
        }

        private float TimeCycle = 0f;
        private float DistanceBetweenSegments = 4f;
        private const int countSegments = 8;
        private float CircleSize = 150f;
        private float LineSize = 10f;
        private float XCenter = 1;
        private float YCenter = 1;
        private SizeF textSize1;
        private SizeF textSize2;
        private SizeF textSize3;

        private string text1 = "Загрузка.";
        private string text2 = "Загрузка..";
        private string text3 = "Загрузка...";
        private int textnum = 0;

        private void FormLoad_Load(object sender, EventArgs e)
        {
            XCenter = pictureBoxLoadAnim.Width / 2;
            YCenter = pictureBoxLoadAnim.Height / 2;

            //Directory.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Cookies), true);

            new Action(LocalData.LoadDictionaries).BeginInvoke(new AsyncCallback(OnLoadDictionariesFinish), null);

            Graphics g_measureText = Graphics.FromHwnd(IntPtr.Zero);

            textSize1 = g_measureText.MeasureString("Загрузка.", DefaultFont);
            textSize2 = g_measureText.MeasureString("Загрузка..", DefaultFont);
            textSize3 = g_measureText.MeasureString("Загрузка...", DefaultFont);
        }

        private void OnLoadDictionariesFinish(IAsyncResult res)
        {
            if (!API.IsLastRequestInternetError)
            {
                Thread thr = new Thread(new ThreadStart(() =>
                {
                    Application.Run(new FormEnter());
                }));
                thr.SetApartmentState(ApartmentState.STA);
                thr.Start();
                this.Invoke(new Action(() => { this.Close(); }));
            }
            else
            {
                MessageBox.Show("Не удалось связаться с сервером.");
            }
        }

        private void pictureBoxLoadAnim_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < countSegments; i++)
            {
                e.Graphics.FillPie(i % 2 == 0 ? Brushes.LightPink : Brushes.GreenYellow, XCenter - CircleSize / 2, YCenter - CircleSize / 2, CircleSize, CircleSize, (360f / countSegments) * i + TimeCycle + DistanceBetweenSegments, 360 / countSegments - DistanceBetweenSegments);
            }
            e.Graphics.FillPie(Brushes.White, XCenter - CircleSize / 2 + LineSize, YCenter - CircleSize / 2 + LineSize, CircleSize - 2 * LineSize, CircleSize - 2 * LineSize, 0, 360);

            if (textnum == 1)
            {
                e.Graphics.DrawString(text1, DefaultFont, Brushes.Black, XCenter - textSize1.Width / 2, YCenter - textSize1.Height / 2);
            }
            else if (textnum == 2)
            {
                e.Graphics.DrawString(text2, DefaultFont, Brushes.Black, XCenter - textSize2.Width / 2, YCenter - textSize2.Height / 2);
            }
            else
            {
                e.Graphics.DrawString(text3, DefaultFont, Brushes.Black, XCenter - textSize3.Width / 2, YCenter - textSize3.Height / 2);
            }
        }

        private void timerUpdater_Tick(object sender, EventArgs e)
        {
            TimeCycle += 4f;
            textnum = (int)(TimeCycle / 120) % 3 + 1;
            pictureBoxLoadAnim.Invalidate();
        }
    }
}
