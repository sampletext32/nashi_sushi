﻿using SushiLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace OperatorAPP
{
    public class LocalData
    {
        public static User CurrentUser = null;
        public static Point_Work CurrentPoint_Work = null;
        #region Dictionaries
        public static List<User> users = new List<User>();
        public static List<AdvPoint> advpoints = new List<AdvPoint>();
        public static List<City> cities = new List<City>();
        public static List<Client> clients = new List<Client>();
        public static List<Dolgnost> dolgnosti = new List<Dolgnost>();
        public static List<Order_State> order_states = new List<Order_State>();
        public static List<Point_Work> points_work = new List<Point_Work>();
        public static List<Product> products = new List<Product>();
        public static List<Rayon> rayons = new List<Rayon>();
        public static List<Street> streets = new List<Street>();
        #endregion

        public static void LoadDictionaries()
        {
            //Здесь задержка для теста формы загрузки
            //Thread.Sleep(5000);
            API.users_select_all().ContinueWith(        new Action<Task<List<User>>>(t => {             users = t.Result;           }));
            API.advpoints_select_all().ContinueWith(    new Action<Task<List<AdvPoint>>>((t) => {       advpoints = t.Result;       }));
            API.cities_select_all().ContinueWith(       new Action<Task<List<City>>>((t) => {           cities = t.Result;          }));
            API.clients_select_all().ContinueWith(      new Action<Task<List<Client>>>((t) => {         clients = t.Result;         }));
            API.dolgnosti_select_all().ContinueWith(    new Action<Task<List<Dolgnost>>>((t) => {       dolgnosti = t.Result;       }));
            API.order_states_select_all().ContinueWith( new Action<Task<List<Order_State>>>((t) => {    order_states = t.Result;    }));
            API.points_work_select_all().ContinueWith(  new Action<Task<List<Point_Work>>>((t) => {     points_work = t.Result;     }));
            API.products_select_all().ContinueWith(     new Action<Task<List<Product>>>((t) => {        products = t.Result;        }));
            API.rayons_select_all().ContinueWith(       new Action<Task<List<Rayon>>>((t) => {          rayons = t.Result;          }));
            API.streets_select_all().ContinueWith(      new Action<Task<List<Street>>>((t) => {         streets = t.Result;         }));
        }
        public static async void ReloadUsers()
        {
            await API.users_select_all().ContinueWith(        new Action<Task<List<User>>>(t => {             users = t.Result;           }));
        }
    }
}
