﻿using SushiLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace OperatorAPP
{
    public partial class EditOrderForm : Form
    {
        public EditOrderForm()
        {
            Graphics dpiGraphics = Graphics.FromHwnd(IntPtr.Zero);
            this.AutoScaleDimensions = new SizeF(dpiGraphics.DpiX, dpiGraphics.DpiX);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            dpiGraphics.Dispose();
            InitializeComponent();
        }

        private bool isNowCheckedChanging = false;

        private int CurrentOrderIndex = -1;

        //Список заказов
        private List<Order> last24h_Orders = new List<Order>();

        //Список продуктов к заказам
        private List<List<Order_Product>> last24h_OrderProducts = new List<List<Order_Product>>();

        private void RefillUsersComboBoxes()
        {
            comboBoxSelectPovar.Items.Clear();
            comboBoxSelectCurer.Items.Clear();
            comboBoxSelectUpakov.Items.Clear();

            comboBoxSelectPovar.Items.AddRange(LocalData.users.Where(t =>
                    t.id_dolgnost == (LocalData.dolgnosti.FirstOrDefault(d => d.name == "Повар")?.id ?? 0) &&
                    t.id_point_work == LocalData.CurrentPoint_Work.id &&
                    t.online == 1).ToArray());
            comboBoxSelectCurer.Items.AddRange(LocalData.users.Where(t =>
                    t.id_dolgnost == (LocalData.dolgnosti.FirstOrDefault(d => d.name == "Курьер")?.id ?? 0) &&
                    t.online == 1).ToArray());
            comboBoxSelectUpakov.Items.AddRange(LocalData.users.Where(t =>
                    t.id_dolgnost == (LocalData.dolgnosti.FirstOrDefault(d => d.name == "Упаковщик")?.id ?? 0) &&
                    t.id_point_work == LocalData.CurrentPoint_Work.id &&
                    t.online == 1).ToArray());

            if (comboBoxSelectPovar.Items.Count > 0)
            {
                comboBoxSelectPovar.SelectedIndex = 0;
            }
            else
            {
                comboBoxSelectPovar.Text = string.Empty;
            }
            if (comboBoxSelectCurer.Items.Count > 0)
            {
                comboBoxSelectCurer.SelectedIndex = 0;
            }
            else
            {
                comboBoxSelectCurer.Text = string.Empty;
            }
            if (comboBoxSelectUpakov.Items.Count > 0)
            {
                comboBoxSelectUpakov.SelectedIndex = 0;
            }
            else
            {
                comboBoxSelectUpakov.Text = string.Empty;
            }
        }

        private void OnCellClick(int index)
        {
            if (index == -1) { return; }
            CurrentOrderIndex = index;
            dataGridViewInfoAboutOrder.Rows.Clear();
            last24h_OrderProducts[index].ForEach(t =>
            {
                Product p = LocalData.products.FirstOrDefault(k => k.id == t.id_product);
                dataGridViewInfoAboutOrder.Rows.Add(p?.name ?? "unknown", t?.count ?? 0, p?.price ?? 0, p?.price * t.count, p?.ingredients ?? "");
            });

            comboBoxSelectPovar.Items.Clear();
            comboBoxSelectUpakov.Items.Clear();
            comboBoxSelectCurer.Items.Clear();

            User[] onlinePovars = LocalData.users.Where(t =>
                    t.id_dolgnost == (LocalData.dolgnosti.FirstOrDefault(d => d.name == "Повар")?.id ?? 0) &&
                    t.id_point_work == last24h_Orders[index].id_point_work &&
                    t.online == 1).ToArray();
            User povar = LocalData.users.FirstOrDefault(t => t.id == last24h_Orders[index].id_povar);
            if (onlinePovars.Length == 0)
            {
                label2.Text = povar?.last_name + " " + povar?.first_name;
                label2.Visible = true;
            }
            else
            {
                comboBoxSelectPovar.Items.AddRange(onlinePovars);
                comboBoxSelectPovar.SelectedItem = povar;
                label2.Visible = false;
            }
            User[] onlineCurers = LocalData.users.Where(t =>
                    t.id_dolgnost == (LocalData.dolgnosti.FirstOrDefault(d => d.name == "Курьер")?.id ?? 0) &&
                    t.online == 1).ToArray();
            User kurer = LocalData.users.FirstOrDefault(t => t.id == last24h_Orders[index].id_kurer);
            if (onlineCurers.Length == 0)
            {
                label3.Text = kurer?.last_name + " " + kurer?.first_name;
                label3.Visible = true;
            }
            else
            {
                comboBoxSelectCurer.Items.AddRange(onlineCurers);
                comboBoxSelectCurer.SelectedItem = kurer;
                label3.Visible = false;
            }

            User[] onlineUpakovs = LocalData.users.Where(t =>
                    t.id_dolgnost == (LocalData.dolgnosti.FirstOrDefault(d => d.name == "Упаковщик")?.id ?? 0) &&
                    t.id_point_work == last24h_Orders[index].id_point_work &&
                    t.online == 1).ToArray();
            User upakov = LocalData.users.FirstOrDefault(t => t.id == last24h_Orders[index].id_upakov);
            if (onlineUpakovs.Length == 0)
            {
                label4.Text = upakov?.last_name + " " + upakov?.first_name;
                label4.Visible = true;
            }
            else
            {
                comboBoxSelectUpakov.Items.AddRange(onlineUpakovs);
                comboBoxSelectUpakov.SelectedItem = upakov;
                label4.Visible = false;
            }

            checkBoxIsOperator_Prodavec.Checked = last24h_Orders[index].operator_prodavec == 1;
            checkBoxIsPovar_Upakov.Checked = last24h_Orders[index].povar_upakov == 1;
            checkBoxIsSamovivos.Checked = last24h_Orders[index].samovivoz == 1;
        }

        private async void buttonRefreshTable_Click(object sender, EventArgs e)
        {
            dataGridViewLast24H_Orders.Rows.Clear();
            last24h_Orders = await API.orders_select_last24h_orders();
            if (API.IsLastRequestInternetError)
            {
                MessageBox.Show("Не удалось получить список заказов.\nОшибка сети.");
            }
            for (int i = 0; i < last24h_Orders.Count; i++)
            {
                int id_order = last24h_Orders[i].id;
                last24h_OrderProducts.Add(await API.order_products_select_by_id_order(id_order));
                if (API.IsLastRequestInternetError)
                {
                    MessageBox.Show("Во время загрузки продуктов к заказу " + id_order.ToString() + " произошла ошибка сети.");
                }
                DateTime datetime_start = new DateTime(1970, 1, 1).AddSeconds(last24h_Orders[i].datetime_start).AddHours(3);
                DateTime datetime_finish = new DateTime(1970, 1, 1).AddSeconds(last24h_Orders[i].datetime_finish).AddHours(3);
                string order_state = LocalData.order_states.FirstOrDefault(t => t.id == last24h_Orders[i].id_order_state)?.name ?? "unknown";
                string on_work = last24h_Orders[i].on_work == 1 ? "Да" : "Нет";
                Point_Work point_work = LocalData.points_work.FirstOrDefault(t => t.id == last24h_Orders[i].id_point_work);
                Client client = LocalData.clients.FirstOrDefault(t => t.id == last24h_Orders[i].id_client);
                string clientStr = client?.number_phone/* + " " + client?.last_name + " " + client?.first_name*/;
                string skidka = last24h_Orders[i].skidka.ToString();
                string price_with_skidka = last24h_Orders[i].price_with_skidka.ToString();
                string count_persons = last24h_Orders[i].count_persons.ToString();
                string description = last24h_Orders[i].description;//.Length > 50 ? last24h_Orders[i].description.Substring(0, 50) : last24h_Orders[i].description;
                int row_index = dataGridViewLast24H_Orders.Rows.Add(id_order, datetime_start.Year == 1969 ? "unknown" : datetime_start.ToString(), datetime_finish.Year == 1969 ? "unknown" : datetime_finish.ToString(), order_state, on_work, point_work, clientStr, skidka, price_with_skidka, count_persons, description);
                if (last24h_Orders[i].description.Contains("---Start Edit"))
                {
                    for (int j = 0; j < dataGridViewLast24H_Orders.Rows[row_index].Cells.Count; j++)
                    {
                        dataGridViewLast24H_Orders.Rows[row_index].Cells[j].Style.BackColor = Color.Aqua;
                    }
                }
            }
            if (last24h_Orders.Count != 0)
            {               
                OnCellClick(0);
            }
        }

        private void EditOrderForm_Load(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridViewLast24H_Orders.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void EditOrderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Thread thr = new Thread(new ThreadStart(() =>
            {
                Application.Run(new PerformOrderForm());
            }));
            thr.SetApartmentState(ApartmentState.STA);
            thr.Start();
        }

        private void dataGridViewOrderProducts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            OnCellClick(index);
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!isNowCheckedChanging)
            {
                isNowCheckedChanging = true;

                // 1 1 1

                if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 1 1 0

                if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 1 0 1

                if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 1 0 0

                if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 0 1 1

                if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 0 1 0

                if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = true;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 0 0 1

                if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = true;
                }

                // 0 0 0

                if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = true;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = true;
                }

                isNowCheckedChanging = false;
            }
        }

        private void buttonCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void buttonUpdateWorkers_Click(object sender, EventArgs e)
        {
            if (last24h_Orders[CurrentOrderIndex].id_order_state==4 || last24h_Orders[CurrentOrderIndex].id_order_state == 5)
            {
                MessageBox.Show("Ошибка. Заказ выполнен или отменён.");
                return;
            }

            User povar = comboBoxSelectPovar.SelectedItem as User;
            User upakov = comboBoxSelectUpakov.SelectedItem as User;
            User kurer = comboBoxSelectCurer.SelectedItem as User;

            bool usePovar = false;
            bool useUpakov = false;
            bool useKurer = false;

            #region CheckBox проверка
            if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                usePovar = false;
                useUpakov = false;
                useKurer = false;
            }

            // 1 1 0

            else if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                usePovar = false;
                useUpakov = false;
                useKurer = false;
            }

            // 1 0 1

            else if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                usePovar = false;
                useUpakov = false;
                useKurer = false;
            }

            // 1 0 0

            else if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                usePovar = false;
                useUpakov = false;
                useKurer = false;
            }

            // 0 1 1

            else if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                usePovar = false;
                useUpakov = true;
                useKurer = false;
            }

            // 0 1 0

            else if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                usePovar = true;
                useUpakov = true;
                useKurer = false;
            }

            // 0 0 1

            else if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                usePovar = false;
                useUpakov = true;
                useKurer = true;
            }

            // 0 0 0

            else if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                usePovar = true;
                useUpakov = true;
                useKurer = true;
            }

            #endregion

            if (povar == null && usePovar)
            {
                MessageBox.Show("Не найден повар");
                return;
            }
            if (upakov == null && useUpakov)
            {
                MessageBox.Show("Не найден упаковщик");
                return;
            }
            if (kurer == null && useKurer)
            {
                MessageBox.Show("Не найден курьер");
                return;
            }
            string Description = last24h_Orders[CurrentOrderIndex].description;
            Description += "\n---Start Edit---\n";
            if (usePovar && povar.id != last24h_Orders[CurrentOrderIndex].id_povar)
            {
                if (last24h_Orders[CurrentOrderIndex].id_order_state == 1 && last24h_Orders[CurrentOrderIndex].on_work == 0)
                {
                    bool povar_update_result = await API.orders_update_choose_new_povar(last24h_Orders[CurrentOrderIndex].id, povar.id, LocalData.CurrentUser.id);
                    if (povar_update_result)
                    {
                        Description += "-Povar: " + last24h_Orders[CurrentOrderIndex].id_povar + " -> " + povar.id.ToString() + "\n";
                        last24h_Orders[CurrentOrderIndex].id_povar = povar.id;
                        MessageBox.Show("Повар успешно обновлён");
                    }
                    else if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время обновления повара произошла ошибка сети");
                    }
                    else
                    {
                        MessageBox.Show("Во время обновления повара произошла ошибка сервера");
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно обновить повара в текущей стадии заказа.");
                }
            }
            if (useUpakov && upakov.id != last24h_Orders[CurrentOrderIndex].id_upakov)
            {
                if ((last24h_Orders[CurrentOrderIndex].id_order_state == 2 && last24h_Orders[CurrentOrderIndex].on_work == 0) ||
                last24h_Orders[CurrentOrderIndex].id_order_state == 1)
                {
                    bool upakov_update_result = await API.orders_update_choose_new_upakov(last24h_Orders[CurrentOrderIndex].id, upakov.id, LocalData.CurrentUser.id);
                    if (upakov_update_result)
                    {
                        Description += "-Upakov: " + last24h_Orders[CurrentOrderIndex].id_upakov + " -> " + upakov.id.ToString() + "\n";
                        last24h_Orders[CurrentOrderIndex].id_upakov = upakov.id;
                        MessageBox.Show("Упаковщик успешно обновлён");
                    }
                    else if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время обновления упаковщика произошла ошибка сети");
                    }
                    else
                    {
                        MessageBox.Show("Во время обновления упаковщика произошла ошибка сервера");
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно обновить упаковщика в текущей стадии заказа.");
                }
            }
            if (useKurer && kurer.id != last24h_Orders[CurrentOrderIndex].id_kurer)
            {
                if (useKurer && ((last24h_Orders[CurrentOrderIndex].id_order_state == 3 && last24h_Orders[CurrentOrderIndex].on_work == 0) ||
                last24h_Orders[CurrentOrderIndex].id_order_state <= 2))
                {
                    bool kurer_update_result = await API.orders_update_choose_new_kurer(last24h_Orders[CurrentOrderIndex].id, kurer.id, LocalData.CurrentUser.id);
                    if (kurer_update_result)
                    {
                        Description += "-Kurer: " + last24h_Orders[CurrentOrderIndex].id_kurer + " -> " + kurer.id.ToString() + "\n";
                        last24h_Orders[CurrentOrderIndex].id_kurer = kurer.id;
                        MessageBox.Show("Курьер успешно обновлён");
                    }
                    else if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время обновления курьера произошла ошибка сети");
                    }
                    else
                    {
                        MessageBox.Show("Во время обновления упаковщика произошла ошибка сервера");
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно обновить курьера в текущей стадии заказа.");
                }
            }
            Description += "---End Edit\n";
            last24h_Orders[CurrentOrderIndex].description = Description;
            bool update_Description_result = await API.orders_update_description(last24h_Orders[CurrentOrderIndex].id, Description, LocalData.CurrentUser.id);
            if (update_Description_result)
            {
                MessageBox.Show("Описание успешно обновлено");
            }
            else if (API.IsLastRequestInternetError)
            {
                MessageBox.Show("Во время обновления описания произошла ошибка сети");
            }
            else
            {
                MessageBox.Show("Во время обновления описания произошла ошибка сервера");
            }
            MessageBox.Show("Завершено обновление исполнителей");
            buttonRefreshTable.PerformClick();
        }

        private async void buttonCancelCurrentOrder_Click(object sender, EventArgs e)
        {
            if (last24h_Orders[CurrentOrderIndex].id_order_state == 4 || last24h_Orders[CurrentOrderIndex].id_order_state == 5)
            {
                MessageBox.Show("Ошибка. Заказ выполнен или отменён.");
                return;
            }

            bool cancel_order_result = await API.orders_update_cancel_order(last24h_Orders[CurrentOrderIndex].id, LocalData.CurrentUser.id);
            if (API.IsLastRequestInternetError)
            {
                MessageBox.Show("Во время отмены заказа произошла ошибка сети");
            }
            else
            {
                if (cancel_order_result)
                {
                    MessageBox.Show("Заказ успешно отменён");
                }
                else
                {
                    MessageBox.Show("Не удалось отменить заказ");
                }
            }
            buttonRefreshTable.PerformClick();
        }

        private void buttonReloadUsers_Click(object sender, EventArgs e)
        {
            LocalData.ReloadUsers();
            OnCellClick(CurrentOrderIndex);
        }

        private async void buttonFinishCurrentOrder_Click(object sender, EventArgs e)
        {
            if (last24h_Orders[CurrentOrderIndex].id_order_state == 4 || last24h_Orders[CurrentOrderIndex].id_order_state == 5)
            {
                MessageBox.Show("Ошибка. Заказ выполнен или отменён.");
                return;
            }

            bool cancel_order_result = await API.orders_update_finish_order(last24h_Orders[CurrentOrderIndex].id, LocalData.CurrentUser.id);
            if (API.IsLastRequestInternetError)
            {
                MessageBox.Show("Во время завершения заказа произошла ошибка сети");
            }
            else
            {
                if (cancel_order_result)
                {
                    MessageBox.Show("Заказ успешно завершён");
                }
                else
                {
                    MessageBox.Show("Не удалось завершить заказ");
                }
            }
            buttonRefreshTable.PerformClick();
        }
    }
}
