﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using SushiLib;
using System.Threading;

namespace OperatorAPP
{
    public partial class FormEnter : Form
    {
        public FormEnter()
        {
            Graphics dpiGraphics = Graphics.FromHwnd(IntPtr.Zero);
            this.AutoScaleDimensions = new SizeF(dpiGraphics.DpiX, dpiGraphics.DpiX);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            dpiGraphics.Dispose();
            InitializeComponent();
        }

        private void FormEnter_Load(object sender, EventArgs e)
        {
            if (LocalData.points_work.Count == 0)
            {
                MessageBox.Show("Точки работы не были загружены!");
                return;
            }
            comboBoxPoint_Work.Items.AddRange(LocalData.points_work.ToArray());
        }

        private async void buttonEnter_Click(object sender, EventArgs e)
        {
            string Login = maskedTextBoxLogin.Text;
            string Password = maskedTextBoxPassword.Text;
            Point_Work selectedPoint_Work = (Point_Work)comboBoxPoint_Work.SelectedItem;
            int Point_Work_ID = selectedPoint_Work?.id ?? -1;

            if (Login.Length == 0)
            {
                MessageBox.Show("Не заполнен логин!");
                return;
            }
            if (Password.Length == 0)
            {
                MessageBox.Show("Не заполнен пароль!");
                return;
            }
            if (Point_Work_ID == -1)
            {
                MessageBox.Show("Не выбрана точка работы!");
                return;
            }
            User user = await API.users_select_by_login_password(Login, Password);

            if (user == null)
            {
                if (API.IsLastRequestInternetError)
                {
                    MessageBox.Show("Ошибка соединения");
                }
                else
                {
                    MessageBox.Show("Ошибка входа");
                }
                return;
            }
            else
            {
                int Operator_Dolgnost_ID = LocalData.dolgnosti.FirstOrDefault(t => t.name == "Оператор")?.id ?? -1;
                if (Operator_Dolgnost_ID == -1)
                {
                    MessageBox.Show("В списке должностей не найден оператор!\nВход отменён");
                    return;
                }
                if (user.id_dolgnost != Operator_Dolgnost_ID)
                {
                    MessageBox.Show("Выбранный пользователь не является оператором");
                    return;
                }
                bool start_work_result = await API.users_update_start_work(user.id, Point_Work_ID);
                if (API.IsLastRequestInternetError)
                {
                    MessageBox.Show("Не удалось установить онлайн для этого пользователя");
                    return;
                }

                //Поиск пользователя по ID  в Локальной БД
                User usr = (LocalData.users.FirstOrDefault(t => t.id == user.id));
                if (usr == null)
                {
                    MessageBox.Show("В локальной базе данных не обнаружен текущий пользователь.\nЭто вызвано системной ошибкой и \nне является нормальной реакцией программы.\nРекомендуем связаться с разработчиком для выяснения причины ошибки.\nВход прерван!");
                    bool finish_work_result = await API.users_update_finish_work(user.id);
                    if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Не удалось выполнить критический выход из системы для данного пользователя!");
                        return;
                    }
                    return;
                }
                usr.online = 1;
                MessageBox.Show("Вход выполнен успешно.");

                LocalData.CurrentUser = usr;
                LocalData.CurrentPoint_Work = selectedPoint_Work;

                Thread thr = new Thread(new ThreadStart(() =>
                {
                    Application.Run(new PerformOrderForm());
                }));
                thr.SetApartmentState(ApartmentState.STA);
                thr.Start();
                this.Close();
            }
        }
    }
}
