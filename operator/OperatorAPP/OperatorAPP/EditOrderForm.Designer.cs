﻿using System;

namespace OperatorAPP
{
    partial class EditOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewLast24H_Orders = new System.Windows.Forms.DataGridView();
            this.buttonRefreshTable = new System.Windows.Forms.Button();
            this.buttonCancelCurrentOrder = new System.Windows.Forms.Button();
            this.buttonUpdateWorkers = new System.Windows.Forms.Button();
            this.buttonCloseWindow = new System.Windows.Forms.Button();
            this.checkBoxIsSamovivos = new System.Windows.Forms.CheckBox();
            this.checkBoxIsPovar_Upakov = new System.Windows.Forms.CheckBox();
            this.checkBoxIsOperator_Prodavec = new System.Windows.Forms.CheckBox();
            this.comboBoxSelectCurer = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectUpakov = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectPovar = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewInfoAboutOrder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonReloadUsers = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonFinishCurrentOrder = new System.Windows.Forms.Button();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLast24H_Orders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfoAboutOrder)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewLast24H_Orders
            // 
            this.dataGridViewLast24H_Orders.AllowUserToAddRows = false;
            this.dataGridViewLast24H_Orders.AllowUserToDeleteRows = false;
            this.dataGridViewLast24H_Orders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLast24H_Orders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewLast24H_Orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLast24H_Orders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column12,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewLast24H_Orders.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewLast24H_Orders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewLast24H_Orders.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewLast24H_Orders.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridViewLast24H_Orders.MultiSelect = false;
            this.dataGridViewLast24H_Orders.Name = "dataGridViewLast24H_Orders";
            this.dataGridViewLast24H_Orders.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLast24H_Orders.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewLast24H_Orders.RowHeadersVisible = false;
            this.dataGridViewLast24H_Orders.RowTemplate.Height = 24;
            this.dataGridViewLast24H_Orders.Size = new System.Drawing.Size(738, 122);
            this.dataGridViewLast24H_Orders.TabIndex = 6;
            this.dataGridViewLast24H_Orders.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOrderProducts_CellClick);
            // 
            // buttonRefreshTable
            // 
            this.buttonRefreshTable.Location = new System.Drawing.Point(462, 136);
            this.buttonRefreshTable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonRefreshTable.Name = "buttonRefreshTable";
            this.buttonRefreshTable.Size = new System.Drawing.Size(282, 32);
            this.buttonRefreshTable.TabIndex = 7;
            this.buttonRefreshTable.Text = "Обновить таблицу";
            this.buttonRefreshTable.UseVisualStyleBackColor = true;
            this.buttonRefreshTable.Click += new System.EventHandler(this.buttonRefreshTable_Click);
            // 
            // buttonCancelCurrentOrder
            // 
            this.buttonCancelCurrentOrder.Location = new System.Drawing.Point(166, 482);
            this.buttonCancelCurrentOrder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonCancelCurrentOrder.Name = "buttonCancelCurrentOrder";
            this.buttonCancelCurrentOrder.Size = new System.Drawing.Size(186, 32);
            this.buttonCancelCurrentOrder.TabIndex = 50;
            this.buttonCancelCurrentOrder.Text = "Отменить выбранный заказ";
            this.buttonCancelCurrentOrder.UseVisualStyleBackColor = true;
            this.buttonCancelCurrentOrder.Click += new System.EventHandler(this.buttonCancelCurrentOrder_Click);
            // 
            // buttonUpdateWorkers
            // 
            this.buttonUpdateWorkers.Location = new System.Drawing.Point(6, 482);
            this.buttonUpdateWorkers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonUpdateWorkers.Name = "buttonUpdateWorkers";
            this.buttonUpdateWorkers.Size = new System.Drawing.Size(156, 32);
            this.buttonUpdateWorkers.TabIndex = 51;
            this.buttonUpdateWorkers.Text = "Обновить исполнителей";
            this.buttonUpdateWorkers.UseVisualStyleBackColor = true;
            this.buttonUpdateWorkers.Click += new System.EventHandler(this.buttonUpdateWorkers_Click);
            // 
            // buttonCloseWindow
            // 
            this.buttonCloseWindow.Location = new System.Drawing.Point(633, 482);
            this.buttonCloseWindow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonCloseWindow.Name = "buttonCloseWindow";
            this.buttonCloseWindow.Size = new System.Drawing.Size(111, 32);
            this.buttonCloseWindow.TabIndex = 52;
            this.buttonCloseWindow.Text = "Закрыть окно";
            this.buttonCloseWindow.UseVisualStyleBackColor = true;
            this.buttonCloseWindow.Click += new System.EventHandler(this.buttonCloseWindow_Click);
            // 
            // checkBoxIsSamovivos
            // 
            this.checkBoxIsSamovivos.AutoSize = true;
            this.checkBoxIsSamovivos.Enabled = false;
            this.checkBoxIsSamovivos.Location = new System.Drawing.Point(270, 20);
            this.checkBoxIsSamovivos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxIsSamovivos.Name = "checkBoxIsSamovivos";
            this.checkBoxIsSamovivos.Size = new System.Drawing.Size(85, 17);
            this.checkBoxIsSamovivos.TabIndex = 47;
            this.checkBoxIsSamovivos.Text = "Самовывоз";
            this.checkBoxIsSamovivos.UseVisualStyleBackColor = true;
            this.checkBoxIsSamovivos.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxIsPovar_Upakov
            // 
            this.checkBoxIsPovar_Upakov.AutoSize = true;
            this.checkBoxIsPovar_Upakov.Enabled = false;
            this.checkBoxIsPovar_Upakov.Location = new System.Drawing.Point(138, 20);
            this.checkBoxIsPovar_Upakov.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxIsPovar_Upakov.Name = "checkBoxIsPovar_Upakov";
            this.checkBoxIsPovar_Upakov.Size = new System.Drawing.Size(120, 17);
            this.checkBoxIsPovar_Upakov.TabIndex = 48;
            this.checkBoxIsPovar_Upakov.Text = "Повар-Упаковщик";
            this.checkBoxIsPovar_Upakov.UseVisualStyleBackColor = true;
            this.checkBoxIsPovar_Upakov.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxIsOperator_Prodavec
            // 
            this.checkBoxIsOperator_Prodavec.AutoSize = true;
            this.checkBoxIsOperator_Prodavec.Enabled = false;
            this.checkBoxIsOperator_Prodavec.Location = new System.Drawing.Point(6, 20);
            this.checkBoxIsOperator_Prodavec.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxIsOperator_Prodavec.Name = "checkBoxIsOperator_Prodavec";
            this.checkBoxIsOperator_Prodavec.Size = new System.Drawing.Size(128, 17);
            this.checkBoxIsOperator_Prodavec.TabIndex = 49;
            this.checkBoxIsOperator_Prodavec.Text = "Оператор-Продавец";
            this.checkBoxIsOperator_Prodavec.UseVisualStyleBackColor = true;
            this.checkBoxIsOperator_Prodavec.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // comboBoxSelectCurer
            // 
            this.comboBoxSelectCurer.FormattingEnabled = true;
            this.comboBoxSelectCurer.Location = new System.Drawing.Point(270, 84);
            this.comboBoxSelectCurer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxSelectCurer.Name = "comboBoxSelectCurer";
            this.comboBoxSelectCurer.Size = new System.Drawing.Size(115, 21);
            this.comboBoxSelectCurer.TabIndex = 44;
            // 
            // comboBoxSelectUpakov
            // 
            this.comboBoxSelectUpakov.FormattingEnabled = true;
            this.comboBoxSelectUpakov.Location = new System.Drawing.Point(138, 84);
            this.comboBoxSelectUpakov.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxSelectUpakov.Name = "comboBoxSelectUpakov";
            this.comboBoxSelectUpakov.Size = new System.Drawing.Size(127, 21);
            this.comboBoxSelectUpakov.TabIndex = 45;
            // 
            // comboBoxSelectPovar
            // 
            this.comboBoxSelectPovar.FormattingEnabled = true;
            this.comboBoxSelectPovar.Location = new System.Drawing.Point(6, 84);
            this.comboBoxSelectPovar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxSelectPovar.Name = "comboBoxSelectPovar";
            this.comboBoxSelectPovar.Size = new System.Drawing.Size(127, 21);
            this.comboBoxSelectPovar.TabIndex = 46;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(270, 65);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "Курьер";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(138, 65);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(66, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Упаковщик";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 65);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 13);
            this.label25.TabIndex = 43;
            this.label25.Text = "Повар";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 169);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Сведения о текущем заказе";
            // 
            // dataGridViewInfoAboutOrder
            // 
            this.dataGridViewInfoAboutOrder.AllowUserToAddRows = false;
            this.dataGridViewInfoAboutOrder.AllowUserToDeleteRows = false;
            this.dataGridViewInfoAboutOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewInfoAboutOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewInfoAboutOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInfoAboutOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column11,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewInfoAboutOrder.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewInfoAboutOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewInfoAboutOrder.Location = new System.Drawing.Point(6, 188);
            this.dataGridViewInfoAboutOrder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridViewInfoAboutOrder.MultiSelect = false;
            this.dataGridViewInfoAboutOrder.Name = "dataGridViewInfoAboutOrder";
            this.dataGridViewInfoAboutOrder.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewInfoAboutOrder.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewInfoAboutOrder.RowHeadersVisible = false;
            this.dataGridViewInfoAboutOrder.RowTemplate.Height = 24;
            this.dataGridViewInfoAboutOrder.Size = new System.Drawing.Size(738, 122);
            this.dataGridViewInfoAboutOrder.TabIndex = 38;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Название";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 82;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Количество";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 91;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Цена";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 58;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Стоимость";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 87;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Состав";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 68;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonReloadUsers);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.checkBoxIsOperator_Prodavec);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.checkBoxIsSamovivos);
            this.groupBox1.Controls.Add(this.comboBoxSelectPovar);
            this.groupBox1.Controls.Add(this.checkBoxIsPovar_Upakov);
            this.groupBox1.Controls.Add(this.comboBoxSelectUpakov);
            this.groupBox1.Controls.Add(this.comboBoxSelectCurer);
            this.groupBox1.Location = new System.Drawing.Point(6, 332);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(390, 136);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Исполнители";
            // 
            // buttonReloadUsers
            // 
            this.buttonReloadUsers.Location = new System.Drawing.Point(6, 110);
            this.buttonReloadUsers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonReloadUsers.Name = "buttonReloadUsers";
            this.buttonReloadUsers.Size = new System.Drawing.Size(378, 20);
            this.buttonReloadUsers.TabIndex = 51;
            this.buttonReloadUsers.Text = "Перезагрузить исполнителей";
            this.buttonReloadUsers.UseVisualStyleBackColor = true;
            this.buttonReloadUsers.Click += new System.EventHandler(this.buttonReloadUsers_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(270, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 50;
            this.label4.Text = "label4";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(138, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 50;
            this.label3.Text = "label3";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // buttonFinishCurrentOrder
            // 
            this.buttonFinishCurrentOrder.Location = new System.Drawing.Point(357, 482);
            this.buttonFinishCurrentOrder.Name = "buttonFinishCurrentOrder";
            this.buttonFinishCurrentOrder.Size = new System.Drawing.Size(253, 32);
            this.buttonFinishCurrentOrder.TabIndex = 54;
            this.buttonFinishCurrentOrder.Text = "Принудительно завершить выбранный заказ";
            this.buttonFinishCurrentOrder.UseVisualStyleBackColor = true;
            this.buttonFinishCurrentOrder.Click += new System.EventHandler(this.buttonFinishCurrentOrder_Click);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Id заказа";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Дата начала";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 96;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Дата конца";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 91;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Статус";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 66;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "В работе";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 77;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Точка работы";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 102;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Клиент";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 68;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Скидка";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 69;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Цена со скидкой";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 108;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Кол-во персон";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 96;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Примечание";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 95;
            // 
            // EditOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 537);
            this.Controls.Add(this.buttonFinishCurrentOrder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancelCurrentOrder);
            this.Controls.Add(this.buttonUpdateWorkers);
            this.Controls.Add(this.buttonCloseWindow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewInfoAboutOrder);
            this.Controls.Add(this.buttonRefreshTable);
            this.Controls.Add(this.dataGridViewLast24H_Orders);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "EditOrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование заказов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditOrderForm_FormClosing);
            this.Load += new System.EventHandler(this.EditOrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLast24H_Orders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfoAboutOrder)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.DataGridView dataGridViewLast24H_Orders;
        private System.Windows.Forms.Button buttonRefreshTable;
        private System.Windows.Forms.Button buttonCancelCurrentOrder;
        private System.Windows.Forms.Button buttonUpdateWorkers;
        private System.Windows.Forms.Button buttonCloseWindow;
        private System.Windows.Forms.CheckBox checkBoxIsSamovivos;
        private System.Windows.Forms.CheckBox checkBoxIsPovar_Upakov;
        private System.Windows.Forms.CheckBox checkBoxIsOperator_Prodavec;
        private System.Windows.Forms.ComboBox comboBoxSelectCurer;
        private System.Windows.Forms.ComboBox comboBoxSelectUpakov;
        private System.Windows.Forms.ComboBox comboBoxSelectPovar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewInfoAboutOrder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonReloadUsers;
        private System.Windows.Forms.Button buttonFinishCurrentOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
    }
}