package com.dubrovin.povar.common.utils;

import android.content.Context;
import android.content.Intent;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.PointWork;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by NikDubrovin on 04.12.2017.
 */

public class OutUtils {


    /**
     * @param str String
     * @param mas_str  String mas[]
     * @return number str in mas[]
     */
    public static int findNumberString(String str,String[] mas_str) {
            for (int i = 0; i < mas_str.length; i++) {
                if (str.equals(mas_str[i])) {
                     return i;
                }
            }

        return -1;
    }

    public static String replaceAllBlank(String str) {
        if (TextUtils.isEmpty(str)) return "";

        Pattern p = Pattern.compile("\\s*|\t|\r|\n");
        Matcher m = p.matcher(str);
        String dest = m.replaceAll("");
        return dest;
    }

    public static String trimNewLine(String str) {
        if (TextUtils.isEmpty(str)) return "";

        str = str.trim();
        Pattern p = Pattern.compile("\t|\r|\n");
        Matcher m = p.matcher(str);
        String dest = m.replaceAll("");
        return dest;
    }

    /**
     * @param Class to
     * @param packageContext from
     * @param extra_str extra
     * @param put_str value
     * @return
     */
    public static Intent newIntent(Context packageContext, Class<?> Class, String extra_str, String put_str, int ... FLAGS) {
        Intent intent = new Intent(packageContext,Class);
        for (int flag : FLAGS)
        intent.setFlags(flag);
        intent.putExtra(extra_str, put_str);
        return intent;
    }

    /**
     * Check for null String : str == null || str.equals("") || str.equals(" ") || TextUtils.isEmpty(str) || str.isEmpty()
     * @param str
     * @return
     */
    public static boolean isStrNull(String str){
        return str == null || str.equals("") || str.equals(" ") || TextUtils.isEmpty(str) || str.isEmpty();
    }

    /**
     *  Parsing created_at date <p>
     * "2017-09-26T13:49:08Z"
     *
     * @param dateStr
     * @return String Date
     */
    public static String formatDate(String dateStr) {
        dateStr = dateStr.substring(0,10);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-DD");
        SimpleDateFormat simpleDateFormatOut = new SimpleDateFormat("EEE, d MMM yyyy");
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateStr);
            String formatDate = simpleDateFormatOut.format(date);
            return formatDate;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  return List<String> from List<PointWork> to Spinner
     *
     * @param pointWorks
     * @return List<String>
     */
    public static List<String> getListNameSpinner(List<PointWork> pointWorks){
        List<String> strings = new ArrayList<>();
        for (PointWork pointWork : pointWorks) {
            strings.add(pointWork.getName());
        }

        return strings;
    }


    public static String otherParametersOrder(Order mOrder) {
        String string = "";
        try {
            if (Integer.valueOf(mOrder.getPovarUpakov()) == 1)
                string = "Повар-упаковщик";

            if (Integer.valueOf(mOrder.getSamovivoz()) == 1)
                if (isStrNull(string))
                    string.concat("Самовывоз");
                else
                    string.concat(", Самовывоз");

            return string;
        } catch (NullPointerException e){
            e.printStackTrace();
            return "";
        }
    }
}
