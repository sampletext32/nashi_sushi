package com.dubrovin.povar.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.SubmitProcessButton;
import com.dubrovin.povar.R;
import com.dubrovin.povar.common.config.MainConfig;
import com.dubrovin.povar.common.constants.Commands;
import com.dubrovin.povar.common.constants.Constants;
import com.dubrovin.povar.common.constants.TypeView;
import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.common.utils.NetworkUtil;
import com.dubrovin.povar.common.utils.OutUtils;
import com.dubrovin.povar.data.StorageClass;
import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.Status;
import com.dubrovin.povar.data.network.RetrofitSushi;
import com.dubrovin.povar.ui.SushiService;
import com.dubrovin.povar.ui.adapters.OrderAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NikDubrovin on 12/8/2017.
 */

public class CurrentOrdersActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewCurrentOrders;
    private OrderAdapter mOrderAdapter;
    private StorageClass mStorageClass = StorageClass.getInstance();
    private AVLoadingIndicatorView mAVLoadingIndicatorViewRecyclerView;
    private TextView textViewCurrentOrders;
    private SubmitProcessButton submitProcessButtonExitSession;

    private Thread mThreadPause;
    private Handler uiHandler;
    private boolean mPause = false;
    private static int TIME_PAUSE = 2; // seconds

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_orders);

        mStorageClass.setState("current");
        GLog.i("onCreate CurrentOrdersActivity");

        // start service
        startService(new Intent(CurrentOrdersActivity.this, SushiService.class));

        // Load View
        loadView();

        // update ui + start workingOrderActivity
        startThreadPause();

        // Listeners
        submitProcessButtonExitSession.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FinishWork(mStorageClass.getUser().getId());

                if(!mStorageClass.isOrdersListEmpty())
                    mStorageClass.getOrdersList().clear();

                stopService(new Intent(getApplicationContext(),SushiService.class));
                Toasty.success(getApplicationContext(),Constants.SUCCESS_EXIT_SYSTEM,Toast.LENGTH_SHORT,true).show();
                Intent intent = OutUtils.newIntent(getApplicationContext(), AuthActivity.class,"","",Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        /**
         *   Update TextView <p>
         *   Update RecyclerView <p
         */
        uiHandler = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        String str = "Текущие заказы : " + mStorageClass.getOrdersList().size();
                        textViewCurrentOrders.setText(str);
                        loadRecyclerView(mStorageClass.getOrdersList());
                        Toasty.info(getApplicationContext(), "началось выполнение заказа", Toast.LENGTH_SHORT, true).show();
                        GLog.i("Hundler Message");
                        return true;
                }
                return false;
            }
        });
    }

    /**
     *  Pause 2 sec <p>
     *  Create While(true) <p>
     *  mStorageClass.getOrdersList().get(0) != null<p>
     */
    private void startThreadPause() {
            mThreadPause = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true) {
                        //Log.i("startThreadPause",".............");
                        if (!mPause && !mStorageClass.isOrdersListEmpty() && mStorageClass.getOrdersList().get(0) != null && !mStorageClass.getState().equals("auth") && !mStorageClass.getState().equals("working")) {
                            // Update UI
                            if (!NetworkUtil.isNetworkAvailable(getApplicationContext()))
                                Toasty.error(getApplicationContext(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
                            else {
                                uiHandler.sendEmptyMessage(1);
                                // Pause
                                try {
                                    TimeUnit.SECONDS.sleep(TIME_PAUSE);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                stopService(new Intent(CurrentOrdersActivity.this, SushiService.class));
                                mStorageClass.setOrderChoose(mStorageClass.getOrdersList().get(0));
                                Intent intent = OutUtils.newIntent(getApplication(), WorkingOrderActivity.class, "", "", Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                // leaving the loop
                                break;
                            }
                        }
                    }
                }
            });

            mThreadPause.start();
    }

    @Override
    public void onBackPressed() { }

    /**
     * reference on view
     */
    private void loadView() {
        // reference on view
        mAVLoadingIndicatorViewRecyclerView = findViewById(R.id.activity_current_orders_recycler_view_loader);
        mRecyclerViewCurrentOrders = findViewById(R.id.current_orders_current_orders_recycler_view);
        textViewCurrentOrders = findViewById(R.id.current_orders_current_orders_text_view);
        submitProcessButtonExitSession = findViewById(R.id.current_orders_close_session_button);
    }

    private void loadRecyclerView(List<Order> orders){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewCurrentOrders.setLayoutManager(linearLayoutManager);
        mRecyclerViewCurrentOrders.setItemAnimator(new DefaultItemAnimator());
        try {
            mOrderAdapter = new OrderAdapter(orders, TypeView.ORDERS);
            mRecyclerViewCurrentOrders.setAdapter(mOrderAdapter);
            mOrderAdapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            e.printStackTrace();
            GLog.i("Error get list orders ----> list order == null");
        }
    }

    /**
     *  @since finish work - users.online = 0
     * @param id
     */
    private void FinishWork(String id)
    {
        RetrofitSushi.getApi().addLogUpdateFinishWork(Commands.USERS_UPDATE_FINISH_WORK,id,MainConfig.KEY).enqueue(new Callback<Status>() {

            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.body()!=null)
                    mStorageClass.getStatusList().add(response.body());
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toasty.warning(getApplicationContext(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    //region RecyclerView hide/show
    private void loaderRecycelrViewHide()
    {
        mAVLoadingIndicatorViewRecyclerView.hide();
        mAVLoadingIndicatorViewRecyclerView.setVisibility(View.GONE);
        mRecyclerViewCurrentOrders.setVisibility(View.VISIBLE);
    }

    private void loaderRecycelrViewShow()
    {
        mAVLoadingIndicatorViewRecyclerView.show();
        mAVLoadingIndicatorViewRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerViewCurrentOrders.setVisibility(View.GONE);
    }
    //endregion

    @Override
    protected void onDestroy() {
        // При любом onDestroy - stopService
        stopService(new Intent(CurrentOrdersActivity.this,SushiService.class));
        mThreadPause.interrupt();
        //FinishWork(mStorageClass.getUser().getId());
        GLog.i("onDestroy CurrentOrdersActivity");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        GLog.i("onResume CurrentOrdersActivity");
        mPause = false;
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GLog.i("onPause CurrentOrdersActivity");
        mPause = true;
    }

    @Override
    protected void onStop() {
        GLog.i("onStop CurrentOrdersActivity");
        super.onStop();
    }
}
