package com.dubrovin.povar.ui.activities;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.dubrovin.povar.R;
import com.dubrovin.povar.common.constants.Commands;
import com.dubrovin.povar.common.config.MainConfig;
import com.dubrovin.povar.common.constants.Constants;
import com.dubrovin.povar.common.constants.TypeView;
import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.common.utils.NetworkUtil;
import com.dubrovin.povar.data.SingletonFonts;
import com.dubrovin.povar.data.StorageClass;
import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.data.models.ProductId;
import com.dubrovin.povar.data.models.Status;
import com.dubrovin.povar.data.network.RetrofitSushi;
import com.dubrovin.povar.ui.adapters.ProductAdapter;
import com.dubrovin.povar.ui.adapters.OrderAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.dd.processbutton.iml.SubmitProcessButton;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class WorkingOrderActivity extends AppCompatActivity {

    private SubmitProcessButton mSubmitProcessButtonCloseOrder;
    private Chronometer mChronometer;
    private RecyclerView mRecyclerViewInfoOrder;
    private RecyclerView mRecyclerViewProducts;
    private TextView mTextViewProducts;
    private StorageClass mStorageClass = StorageClass.getInstance();
    private OrderAdapter mOrderAdapter;
    private ProductAdapter mProductAdapter;
    private AVLoadingIndicatorView mAVLoadingIndicatorView;

    private NotificationManager nm;
    // Check notification && cancel their
    private Thread mThreadCheckNotification;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_working);

        GLog.i("onCreate WorkingOrderActivity");
        mStorageClass.setState("working");

        // UI
        loadView();

        StartOrder();
        // onWork = 1
        StartWorkOrder();

       int idp_event =  getIDsProducts();

       if(idp_event != 1) {
           Toasty.error(getApplication(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
           GLog.i("ProductIds doesnt get  !!!!!");
       }

        // Notification cancel
        try
        {
            nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            if(nm != null)
                nm.cancelAll();
        }
        catch (Exception e){e.printStackTrace();}

        // clear all notification
        mThreadCheckNotification = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(3);
                    nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
                    if(nm != null)
                        nm.cancelAll();
                    GLog.i("NM CANCEL ALL");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mThreadCheckNotification.start();

        // Listeners
        mSubmitProcessButtonCloseOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtil.isNetworkAvailable(getApplicationContext()))
                    finishOrder();
                else
                    Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    private void loadView() {

        mSubmitProcessButtonCloseOrder = findViewById(R.id.order_working_close_order_button);
        mChronometer = findViewById(R.id.order_working_timer_chronometer);
        mRecyclerViewInfoOrder = findViewById(R.id.order_working_info_recycler_view);
        mTextViewProducts = findViewById(R.id.order_working_products_text_view);
        mRecyclerViewProducts = findViewById(R.id.order_working_products_recycler_view);

        mAVLoadingIndicatorView = findViewById(R.id.activity_working_recycler_view_loader);

        mSubmitProcessButtonCloseOrder.setTypeface(SingletonFonts.getInstance(getApplication()).getFont());

        // RecylcerView
        LinearLayoutManager linearLayoutManagerProduct = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewProducts.setLayoutManager(linearLayoutManagerProduct);
        mRecyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager linearLayoutManagerOrder = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewInfoOrder.setLayoutManager(linearLayoutManagerOrder);
        mRecyclerViewInfoOrder.setItemAnimator(new DefaultItemAnimator());

        // Chronometer
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();

        // Info Order
        List<Order> orders = new ArrayList<Order>();
        orders.add(mStorageClass.getOrderChoose());
        mOrderAdapter = new OrderAdapter(orders, TypeView.ORDER);
        mRecyclerViewInfoOrder.setAdapter(mOrderAdapter);
        mOrderAdapter.notifyDataSetChanged();
    }

    /**
     *  POVARS_WORK_UPDATE_ORDER_FINISH <p>
     *
     *  if getPovarUpakov() == 1 <p>
     *      start WorkingPackerActivity <p>
     *  else  <p>
     *      ORDERS_UPDATE_MOVE_ORDER_TO_UPAKOV && start CurrentOrdersActivity
     */
    private void finishOrder() {
        loaderButtonCloseShow();

        if (NetworkUtil.isNetworkAvailable(getApplicationContext())) {
            RetrofitSushi.getApi().addLogUpdateOrderFinish(Commands.POVARS_WORK_UPDATE_ORDER_FINISH, mStorageClass.getOrderChoose().getId(), MainConfig.KEY, StorageClass.getInstance().getUser().getId()).enqueue(new Callback<Status>() {
                @Override
                public void onResponse(Call<Status> call, Response<Status> response) {
                }

                @Override
                public void onFailure(Call<Status> call, Throwable t) {
                    t.printStackTrace();
                    Toasty.warning(getApplicationContext(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
                }
            });


            if (Integer.valueOf(mStorageClass.getOrderChoose().getPovarUpakov()) == 1) {
                if (!mStorageClass.getState().equals("auth") && !mStorageClass.getState().equals("current")) {
                    mStorageClass.getOrdersList().clear();
                    startActivity(new Intent(getApplication(), WorkingPackerActivity.class));
                    Toasty.info(getApplicationContext(), "Работа упаковщика началась", Toast.LENGTH_SHORT, true).show();
                }
            } else {
                RetrofitSushi.getApi().addLogUpdateFinishOrder(Commands.ORDERS_UPDATE_MOVE_ORDER_TO_UPAKOV, mStorageClass.getOrderChoose().getId(), MainConfig.KEY, StorageClass.getInstance().getUser().getId()).enqueue(new Callback<Status>() {
                    @Override
                    public void onResponse(Call<Status> call, Response<Status> response) {
                        if (!mStorageClass.getState().equals("auth") && !mStorageClass.getState().equals("current")) {
                            mStorageClass.getOrdersList().clear();
                            startActivity(new Intent(WorkingOrderActivity.this, CurrentOrdersActivity.class));
                            Toasty.info(getApplicationContext(), "Заказ передан упаковщику", Toast.LENGTH_SHORT, true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Status> call, Throwable t) {
                        Toasty.warning(getApplicationContext(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
                        loaderButtonCloseHide();
                    }
                });
            }
        }
        else
            Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
    }

    private void StartOrder() {
        RetrofitSushi.getApi().addLogUpdateOrderStart(Commands.POVARS_WORK_UPDATE_ORDER_START,mStorageClass.getOrderChoose().getId(), MainConfig.KEY,StorageClass.getInstance().getUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toasty.success(getApplicationContext(), "Робота над заказом началась", Toast.LENGTH_SHORT, true).show();
            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toasty.error(getApplicationContext(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
                t.printStackTrace();
            }
        });
    }

    private void StartWorkOrder() {
        RetrofitSushi.getApi().addLogUpdateOrderStartWork(Commands.ORDERS_UPDATE_ON_WORK_START,mStorageClass.getOrderChoose().getId(), MainConfig.KEY,StorageClass.getInstance().getUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toasty.success(getApplicationContext(), "StartWorkOrder", Toast.LENGTH_SHORT, true).show();
            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toasty.error(getApplicationContext(), "StartWorkOrder", Toast.LENGTH_SHORT, true).show();

            }
        });
    }

    /**
     *    Get listProducts for currentOrder(id)
     */
    private int getIDsProducts() {

        loaderSpinnerShow();

        if (!NetworkUtil.isNetworkAvailable(getApplication())) {
            Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
            loaderSpinnerHide();
            return 0;
        } else {
            LoadProductIds lpd = new LoadProductIds();
            List<ProductId> response = null;
            // if products list isn't empty
            if(!mStorageClass.isProductListEmpty()) {
                try {
                    lpd.execute();
                    response = lpd.get();
                    // check null
                    if (response != null) {
                        if (response.get(0).getId().equals("test_product_id_asysns_task_error")) {
                            Toasty.error(getApplication(), "ошибка при загрузке данных!", Toast.LENGTH_SHORT, true).show();
                            // clear list!
                            response.clear();
                            loaderSpinnerHide();
                        } else {
                            List<Product> newProducts = new ArrayList<>();

                            mStorageClass.setProductIds(response);

                            for (ProductId id : response)
                                for (Product product : mStorageClass.getProductList())
                                    if (product.getId().equals(id.getId_product()))
                                        newProducts.add(product);

                            mProductAdapter = new ProductAdapter(newProducts);
                            mRecyclerViewProducts.setAdapter(mProductAdapter);
                            mProductAdapter.notifyDataSetChanged();

                            loaderSpinnerHide();
                            return 1;
                        }
                    }
                    // if response == null
                    loaderSpinnerHide();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    Toasty.error(getApplication(), "ошибка при загрузке данных!", Toast.LENGTH_SHORT, true).show();
                    loaderSpinnerHide();
                    return 0;
                }
            } else{
                Toasty.error(getApplication(), "ошибка при загрузке данных! Закончите заказ и перезагрузите приложение!", Toast.LENGTH_SHORT, true).show();
                GLog.i("List Products is emtpy --> 3 Activity");
                loaderSpinnerHide();
                return 0;
            }
        }

        return 0;

//        RetrofitSushi.getApi().getProductsIds(Commands.ORDER_PRODUCTS_SELECT_BY_ID_ORDER, mStorageClass.getOrderChoose().getId(), MainConfig.KEY).enqueue(new Callback<List<ProductId>>() {
//        @Override
//        public void onResponse(Call<List<ProductId>> call, Response<List<ProductId>> response) {
//            if(response.body()!=null) {
//                if(mStorageClass.isProductListEmpty()) {
//                    List<Product> newProducts = new ArrayList<>();
//
//                    mStorageClass.setProductIds(response.body());
//
//                    for(ProductId id : response.body())
//                        for (Product product : mStorageClass.getProductList())
//                            if(product.getId().equals(id.getId_product())) {
//                                newProducts.add(product);
//                            }
//
//
//                    mProductAdapter = new ProductAdapter(newProducts);
//                    mRecyclerViewProducts.setAdapter(mProductAdapter);
//                    mProductAdapter.notifyDataSetChanged();
//                }
//                else {
//                    getProducts(response.body());
//                }
//
//            }
//        }
//        @Override
//        public void onFailure(Call<List<ProductId>> call, Throwable t) {
//            t.printStackTrace();
//        }
//    });
    }

    private void getProducts(final List<ProductId> productIds) {

        RetrofitSushi.getApi().getProducts(Commands.PRODUCTS_SELECT_ALL, MainConfig.KEY).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, final Response<List<Product>> response) {
                try {
                    if(response.body() != null) {
                        mStorageClass.setProductList(response.body());

                        List<Product> newProducts = new ArrayList<>();

                        for (Product product : mStorageClass.getProductList()){
                            for(ProductId id : productIds)
                                if(product.getId().equals(id.getId_order())) {
                                    newProducts.add(product);
                                }
                        }

                        ProductAdapter mAdapterProduct = new ProductAdapter(newProducts);
                        mRecyclerViewProducts.setAdapter(mAdapterProduct);
                        mAdapterProduct.notifyDataSetChanged();
                    }

                    GLog.i("Error get Products");
                }
                catch (NullPointerException e) {
                    Toasty.warning(getApplicationContext(), Constants.ERROR_PRODUCTS, Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toasty.warning(getApplicationContext(), Constants.ERROR_PRODUCTS, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    //region Spinner show/hide
    private void loaderSpinnerHide() {
        mAVLoadingIndicatorView.setVisibility(View.GONE);
        mAVLoadingIndicatorView.hide();
    }

    private void loaderSpinnerShow() {
        mAVLoadingIndicatorView.setVisibility(View.VISIBLE);
        mAVLoadingIndicatorView.show();
    }
    // endregion

    private void loaderButtonCloseHide() {
        loaderSpinnerHide();
        mSubmitProcessButtonCloseOrder.setVisibility(View.VISIBLE);
    }

    private void loaderButtonCloseShow() {
        loaderSpinnerShow();
        mSubmitProcessButtonCloseOrder.setVisibility(View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        GLog.i("onResume WorkingOrderActtivity");
        nm.cancelAll();
    }

    @Override
    public void onBackPressed() { }

    public static class LoadProductIds extends AsyncTask<String,Void,List<ProductId>> {
        @Override
        protected List<ProductId> doInBackground(String... strings) {
            List<ProductId> ids = new ArrayList<>();
            ProductId p = new ProductId();
            p.setId("test_product_id_asysns_task_error");
            ids.add(p);
            try {
                ids = RetrofitSushi.getApi().getProductsIds(Commands.ORDER_PRODUCTS_SELECT_BY_ID_ORDER, StorageClass.getInstance().getOrderChoose().getId(), MainConfig.KEY).execute().body();
            }
            catch(Exception e){e.printStackTrace();}
            return ids;
        }
    }
}
