package com.dubrovin.povar.ui.adapters.viewholder;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.dubrovin.povar.R;
import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.data.SingletonFonts;
import com.dubrovin.povar.data.StorageClass;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.data.models.ProductId;
import com.dubrovin.povar.ui.base.BaseViewHolder;
import com.dubrovin.povar.ui.base.BaseViewHolder;

/**
 * Created by Dubrovin on 17.12.2017.
 */

public class ProductHolder extends BaseViewHolder {
    private TextView mNameTextView;
    private TextView price;
    private TextView mCompositionTextView;
    private TextView count;
    private Product mProduct;
    private Typeface font;
    private StorageClass sc = StorageClass.getInstance();

    public ProductHolder(View itemView) {
        super(itemView);

        try {
            font = SingletonFonts.getInstance(itemView.getContext()).getFont();

            mNameTextView = itemView.findViewById(R.id.product_name);
            mNameTextView.setTypeface(font);

            price = itemView.findViewById(R.id.product_price);
            price.setTypeface(font);

            count = itemView.findViewById(R.id.product_count);
            count.setTypeface(font);

            mCompositionTextView = itemView.findViewById(R.id.product_composition);
            mCompositionTextView.setTypeface(font);

        }catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public <T> void bindRepos(T object) {
        mProduct = (Product) object;

        mNameTextView.setText(mProduct.getName());
        price.setText(mProduct.getPrice());
        mCompositionTextView.setText(mProduct.getIngredients());

        GLog.i("count productIds : " + sc.getProductIds().size());
        GLog.i("count products : " + sc.getProductList().size());


        String idP = "0";

        if(sc.getProductList() != null && sc.getProductIds() != null)
            for(ProductId id : sc.getProductIds())
                if(mProduct.getId().equals(id.getId_product())) {
                    idP = id.getCount();
                }
        else
            count.setText("0");

             count.setText(idP);

        GLog.i("id : " + idP);
        }
}
