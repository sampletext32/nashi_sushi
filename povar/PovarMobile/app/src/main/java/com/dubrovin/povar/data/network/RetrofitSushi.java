package com.dubrovin.povar.data.network;

import okhttp3.OkHttpClient;

import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.data.api.SushiApi;
import com.dubrovin.povar.common.config.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.dubrovin.povar.common.*;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by NikDubrovin on 17.09.2017.
 */

public class RetrofitSushi {

    private RetrofitSushi() {}

    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    public static SushiApi getApi() {

        Retrofit retrofit = new Retrofit.Builder()
                .client(buildClient())
                .baseUrl(MainConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SushiApi sushiApi = retrofit.create(SushiApi.class);
        return sushiApi;
    }
}
