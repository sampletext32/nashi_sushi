package com.dubrovin.povar.data.api;

import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.PointWork;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.data.models.ProductId;
import com.dubrovin.povar.data.models.Status;
import com.dubrovin.povar.data.models.User;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by NikDubrovin on 17.09.2017.
 */

public interface SushiApi {

    //command=points_work.select.all&parameters
    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<PointWork>> getPointsWork(@Field("command") String command, @Field("key") String key);

    //users.select.by_login_password
    @FormUrlEncoded
    @POST("/api.php/")
    Call<User> signIn(@Field("command") String command, @Field("parameters") JSONObject user, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateStartWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Product>> getProducts(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Order>> getAllOrders(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addOrderToQueue(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateFinishWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderStart(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderFinish(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderStartWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<ProductId>> getProductsIds(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateFinishOrder(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateMoveOrder(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

}
