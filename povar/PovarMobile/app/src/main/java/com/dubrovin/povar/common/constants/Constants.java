package com.dubrovin.povar.common.constants;


/**
 * Created by NikDubrovin on 16.09.2017.
 */

public class Constants {

    public static final String WARING_ERROR  = "ERROR!";

    public static final String ERROR_TEXT_VIEW = "Поле не заполнено";

    public static final String ERROR_OTHER = "Неизвестная ошибка";

    public static final String ERROR_NET_WORK = "Произошла ошибка при запросе на сервер, проверьте доступ к интернету!";

    public static final String ERROR_SIGN_IN = "Не удалось войти";

    public static final String ERROR_POINT_WORKS = "Произошла ошибка при получении точек работы, перезагрузите приложение!";

    public static final String SUCCESS_SIGN_IN = "Вход выполнен";

    public static final String SUCCESS_EXIT_SYSTEM = "Выход из системы выполнен";

    public static final String ERROR_PRODUCTS = "Продукты не были получены";
}
