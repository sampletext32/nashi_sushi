package com.dubrovin.packer.common.constants;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class ErrorsInfo {
    public static final String errorTextView = "Поле не заполнено";
    public static final String errorNotKnowing = "Неизвестная ошибка";
    public static final String errorNetWork = "Произошла ошибка при запросе на сервер, повторите попытку";
    public static final String errorSignIn = "Не удалось войти";
    public static final String errorLogin = "неправильный логин и пароль";
    public static final String errorPointWorks = "Произошла ошибка при получении точек работы, повторите попытку";
    public static final String successSignIn = "Вход выполнен";
    public static final String successExitSystem = "Выход из системы выполнен";
    public static final String errorProducts = "Продукты не были получены";
}
