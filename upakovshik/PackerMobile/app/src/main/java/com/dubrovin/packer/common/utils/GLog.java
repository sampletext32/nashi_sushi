package com.dubrovin.packer.common.utils;

import android.util.Log;

import com.dubrovin.packer.BuildConfig;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

/**
 * Created by Dubrovin on 16.12.2017.
 */

public class GLog {

    private static final String TAG = "UpakClient";

    /**
     * initialize the logger.
     */
    private GLog() {
    }

    static {
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false) // (Optional) Whether to show thread info or not. Default true
                .methodCount(2) // (Optional) How many method line to show. Default 2
            //  .methodOffset(3) // (Optional) Skips some method invokes in stack trace. Default 5
            //  .logStrategy(customLog) // (Optional) Changes the log strategy to print out. Default LogCat
                .tag(TAG) // (Optional) Custom tag for each log. Default PRETTY_LOGGER
                .build();
        Log.i("Static field Logger", "Static Logger");

        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy) {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
    }

    /**
     * log.i
     *
     * @param msg
     */
    public static void i(String msg) {
        if(BuildConfig.DEBUG)
            Logger.i(msg);
    }

    public static void i(int imsg) {
        if(BuildConfig.DEBUG)
            Logger.i(String.valueOf(imsg));
    }

    public static <T> void i(T t){
        if(BuildConfig.DEBUG)
            Logger.i(t.toString());
    }

    /**
     * log.d
     *
     * @param msg
     */
    public static void d(String msg) {
        if(BuildConfig.DEBUG)
            Logger.d(msg);
    }

    /**
     * log.w
     *
     * @param msg
     */
    public static void w(String msg) {
        if(BuildConfig.DEBUG)
            Logger.w(msg);
    }

    /**
     * log.e
     *
     * @param msg
     */
    public static void e(String msg) {
        if(BuildConfig.DEBUG)
            Logger.e(msg);
    }

    public static void e(Throwable e) {
        if(BuildConfig.DEBUG)
            Logger.e(e, "");
    }

    /**
     * @param msg
     */
    public static void json(String msg) {
        if(BuildConfig.DEBUG)
            Logger.json(msg);
    }

    /**
     * @param msg
     */
    public static void xml(String msg) {
        if(BuildConfig.DEBUG)
            Logger.xml(msg);
    }
}
