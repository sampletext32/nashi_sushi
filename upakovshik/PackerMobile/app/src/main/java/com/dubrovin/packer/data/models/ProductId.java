package com.dubrovin.packer.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dubrovin on 18.12.2017.
 */

public class ProductId {
    @SerializedName("id")
    @Expose
    private  String  id;
    @SerializedName("id_order")
    @Expose
    private String id_order;
    @SerializedName("id_product")
    @Expose
    private String id_product;
    @SerializedName("count")
    @Expose
    private String count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
