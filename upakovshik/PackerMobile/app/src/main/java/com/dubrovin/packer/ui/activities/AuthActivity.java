package com.dubrovin.packer.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.FlatButton;
import com.dd.processbutton.ProcessButton;
import com.dubrovin.packer.R;

import com.dubrovin.packer.common.constants.Commands;
import com.dubrovin.packer.common.constants.Config;
import com.dubrovin.packer.common.constants.ErrorsInfo;
import com.dubrovin.packer.common.utils.GLog;
import com.dubrovin.packer.common.utils.NetWorkUtil;
import com.dubrovin.packer.data.App;
import com.dubrovin.packer.data.models.PointWork;
import com.dubrovin.packer.data.models.Status;
import com.dubrovin.packer.data.models.User;
import com.dubrovin.packer.data.network.RetrofitSushi;

import com.wang.avi.AVLoadingIndicatorView;


import org.angmarch.views.NiceSpinner;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends AppCompatActivity {

    private EditText mEditTextUsername;
    private EditText mEditTextPassword;
    private NiceSpinner mNiceSpinner;
    private AVLoadingIndicatorView mAVLoadingIndicatorViewSign;
    private AVLoadingIndicatorView mAVLoadingIndicatorViewSpinner;
    private ProcessButton flatButtonSignIn;

    TextView SpinnerTextView;

   // private StorageClass mStorageClass = StorageClass.getInstance();

 //   private List<PointWork> mPointWorkList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);


        final App app = (App) getApplicationContext();


        if (!NetWorkUtil.isNetworkAvailable(getApplicationContext()))
            Toasty.error(getApplicationContext(), "отсутствует интернет", Toast.LENGTH_SHORT, true).show();

        GLog.i("onCreate AuthActivity");


        mEditTextUsername = findViewById(R.id.username);
        mEditTextPassword = findViewById(R.id.password);


        SpinnerTextView = findViewById(R.id.activity_auth_error_spinner_point_work_text_view);


        mAVLoadingIndicatorViewSign = findViewById(R.id.activity_auth_loader_sign);
        mAVLoadingIndicatorViewSpinner = findViewById(R.id.activity_auth_loader_spinner);


        final FlatButton flatButtonExit = findViewById(R.id.exitAuthActivity);


        flatButtonSignIn = findViewById(R.id.SignInAuthActivity);



        final TextView UserNameTextView = findViewById(R.id.activity_auth_error_user_text_view);



        final TextView PasswordTextView = findViewById(R.id.activity_auth_error_password_text_view);




        mNiceSpinner = findViewById(R.id.nice_spinner);


        //sign in system
        flatButtonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login = mEditTextUsername.getText().toString();
                String password = mEditTextPassword.getText().toString();

                if(login.isEmpty())
                    UserNameTextView.setVisibility(View.VISIBLE);
                else
                    UserNameTextView.setVisibility(View.GONE);

                if(password.isEmpty())
                    PasswordTextView.setVisibility(View.VISIBLE);
                else
                    PasswordTextView.setVisibility(View.GONE);

                if(app.getPointWorkList().isEmpty())
                    SpinnerTextView.setVisibility(View.VISIBLE);
                else
                    SpinnerTextView.setVisibility(View.GONE);

                if(!password.isEmpty() && !login.isEmpty() && !app.getPointWorkList().isEmpty())
                    SignIn(login,password);
                else
                {

                    if(app.getPointWorkList().isEmpty())
                    {
                        Toasty.error(getApplicationContext(), ErrorsInfo.errorPointWorks, Toast.LENGTH_SHORT, true).show();
                        getPointsWork();
                    }
                    Toasty.error(getApplicationContext(), "поля ввода не заполнены", Toast.LENGTH_SHORT, true).show();
                }


            }
        });


        //exit from app
        flatButtonExit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        getPointsWork();
    }

    /**
     * Sign in system :
     * <li>Check pass && login</li>
     * <li> start CurrentOrdersActivity </li>
     * @param UserName
     * @param Password
     */
    private void SignIn(String UserName,String Password)
    {

        //region create jsonObject [login] [password]
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("login",UserName);
            jsonObject.put("password",Password);
        } catch (JSONException e) {
            Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            return;
        }
        //endregion

        loaderSignShow();

        RetrofitSushi.getApi().signIn(Commands.commandLoginInSystem,jsonObject.toString(), Config.KEY).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                loaderSignHide();
                if(response.isSuccessful())
                {
                    try {

                        App app = (App) getApplicationContext();

                        if(!response.body().getIdDolgnost().equals("4"))
                        {
                            Toasty.error(getApplicationContext(), "вход выполнен не упаковщиком", Toast.LENGTH_SHORT, true).show();
                            return;
                        }

                            app.setChoosePointWork(app.getPointWorkList().get(mNiceSpinner.getSelectedIndex()));
                            app.getProducts();
                            app.setChooseUser(response.body());
                            startWorkUser(response.body().getId(),app.getChoosePointWork().getId());

                    }
                    catch (NullPointerException e)
                    {
                        e.printStackTrace();
                        loaderSignHide();
                        if (NetWorkUtil.isNetworkAvailable(getApplicationContext()))
                            Toasty.error(getApplicationContext(), ErrorsInfo.errorLogin, Toast.LENGTH_SHORT, true).show();
                        else Toasty.error(getApplicationContext(), ErrorsInfo.errorNetWork, Toast.LENGTH_SHORT, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                loaderSignHide();
                if (NetWorkUtil.isNetworkAvailable(getApplicationContext()))
                    Toasty.error(getApplicationContext(), ErrorsInfo.errorLogin, Toast.LENGTH_SHORT, true).show();
                else Toasty.error(getApplicationContext(), ErrorsInfo.errorNetWork, Toast.LENGTH_SHORT, true).show();
            }
        });
    }


    private void getPointsWork() {

        loaderSpinnerShow();
        SpinnerTextView.setVisibility(View.GONE);

        RetrofitSushi.getApi().getPointsWork(Commands.commandGetPointWork, Config.KEY).enqueue(new Callback<List<PointWork>>() {
            @Override
            public void onResponse(Call<List<PointWork>> call, Response<List<PointWork>> response) {
                try {
                    if(response.isSuccessful())
                    {

                        App app = (App)getApplicationContext();

                        loaderSpinnerHide();
                        app.getPointWorkList().addAll(response.body());
                        SpinnerTextView.setVisibility(View.GONE);
                        List<String> strings = new ArrayList<>();
                        for (PointWork pointWork:app.getPointWorkList())
                        {
                            strings.add(pointWork.getName());
                        }
                        mNiceSpinner.attachDataSource(strings);
                    }
                }
                catch (NullPointerException e)
                {
                    e.printStackTrace();
                    Toasty.warning(getApplicationContext(), ErrorsInfo.errorPointWorks, Toast.LENGTH_SHORT, true).show();
                    loaderSpinnerHide();
                }

            }

            @Override
            public void onFailure(Call<List<PointWork>> call, Throwable t) {
                t.printStackTrace();
                loaderSpinnerHide();
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorPointWorks, Toast.LENGTH_SHORT, true).show();
            }
        });
    }



    private void startWorkUser(String id,String idPointWork) {


        loaderSignShow();

        //region create jsonObject [login] [password]
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",id);
            jsonObject.put("id_point_work",idPointWork);
        } catch (JSONException e) {
            Toasty.error(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            return;
        }
        //endregion

        RetrofitSushi.getApi().addLogUpdateStartWork(Commands.commandUsersUpdateStartWork,jsonObject.toString(), Config.KEY).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                {
                    loaderSignHide();
                    Toasty.success(getApplicationContext(),ErrorsInfo.successSignIn,Toast.LENGTH_SHORT,true).show();
                    Intent intent = new Intent(AuthActivity.this, CurrentOrdersActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                loaderSignHide();
                t.printStackTrace();
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });
    }



    @Override
    public void onBackPressed() { }


    private void loaderSpinnerHide()
    {
        mAVLoadingIndicatorViewSpinner.hide();
        mAVLoadingIndicatorViewSpinner.setVisibility(View.GONE);
        mNiceSpinner.setVisibility(View.VISIBLE);
    }

    private void loaderSpinnerShow()
    {
        mAVLoadingIndicatorViewSpinner.show();
        mAVLoadingIndicatorViewSpinner.setVisibility(View.VISIBLE);
        mNiceSpinner.setVisibility(View.GONE);
    }

    private void loaderSignHide()
    {
        mAVLoadingIndicatorViewSign.hide();
        mAVLoadingIndicatorViewSign.setVisibility(View.GONE);
        flatButtonSignIn.setVisibility(View.VISIBLE);
        flatButtonSignIn.setClickable(true);
    }

    private void loaderSignShow()
    {
        mAVLoadingIndicatorViewSign.show();
        mAVLoadingIndicatorViewSign.setVisibility(View.VISIBLE);
        flatButtonSignIn.setVisibility(View.GONE);
        flatButtonSignIn.setClickable(false);
    }
}
