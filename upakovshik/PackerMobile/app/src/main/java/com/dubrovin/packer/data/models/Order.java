package com.dubrovin.packer.data.models;


import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dubrovin on 13.12.2017.
 */


public class Order {
    @SerializedName("id")
    @Expose
    private  String id;
    @SerializedName("id_operator")
    @Expose
    private String idOperator;
    @SerializedName("id_povar")
    @Expose
    private String idPovar;
    @SerializedName("id_upakov")
    @Expose
    private String idUpakov;
    @SerializedName("id_kurer")
    @Expose
    private String idKurer;
    @SerializedName("id_client")
    @Expose
    private String idClient;
    @SerializedName("id_point_work")
    @Expose
    private String idPointWork;
    @SerializedName("datetime_start")
    @Expose
    private String datetimeStart;
    @SerializedName("datetime_finish")
    @Expose
    private String datetimeFinish;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("skidka")
    @Expose
    private String skidka;
    @SerializedName("price_with_skidka")
    @Expose
    private String priceWithSkidka;
    @SerializedName("client_money")
    @Expose
    private String clientMoney;
    @SerializedName("count_persons")
    @Expose
    private String countPersons;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("promocode")
    @Expose
    private String promocode;
    @SerializedName("no_cash")
    @Expose
    private String noCash;
    @SerializedName("operator_prodavec")
    @Expose
    private String operatorProdavec;
    @SerializedName("povar_upakov")
    @Expose
    private String povarUpakov;
    @SerializedName("samovivoz")
    @Expose
    private String samovivoz;
    @SerializedName("id_order_state")
    @Expose
    private String idOrderState;
    @SerializedName("on_work")
    @Expose
    private String onWork;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdOperator() {
        return idOperator;
    }

    public void setIdOperator(String idOperator) {
        this.idOperator = idOperator;
    }

    public String getIdPovar() {
        return idPovar;
    }

    public void setIdPovar(String idPovar) {
        this.idPovar = idPovar;
    }

    public String getIdUpakov() {
        return idUpakov;
    }

    public void setIdUpakov(String idUpakov) {
        this.idUpakov = idUpakov;
    }

    public String getIdKurer() {
        return idKurer;
    }

    public void setIdKurer(String idKurer) {
        this.idKurer = idKurer;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdPointWork() {
        return idPointWork;
    }

    public void setIdPointWork(String idPointWork) {
        this.idPointWork = idPointWork;
    }

    public String getDatetimeStart() {
        return datetimeStart;
    }

    public void setDatetimeStart(String datetimeStart) {
        this.datetimeStart = datetimeStart;
    }

    public String getDatetimeFinish() {
        return datetimeFinish;
    }

    public void setDatetimeFinish(String datetimeFinish) {
        this.datetimeFinish = datetimeFinish;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSkidka() {
        return skidka;
    }

    public void setSkidka(String skidka) {
        this.skidka = skidka;
    }

    public String getPriceWithSkidka() {
        return priceWithSkidka;
    }

    public void setPriceWithSkidka(String priceWithSkidka) { this.priceWithSkidka = priceWithSkidka; }

    public String getClientMoney() {
        return clientMoney;
    }

    public void setClientMoney(String clientMoney) {
        this.clientMoney = clientMoney;
    }

    public String getCountPersons() {
        return countPersons;
    }

    public void setCountPersons(String countPersons) {
        this.countPersons = countPersons;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getNoCash() {
        return noCash;
    }

    public void setNoCash(String noCash) {
        this.noCash = noCash;
    }

    public String getOperatorProdavec() {
        return operatorProdavec;
    }

    public void setOperatorProdavec(String operatorProdavec) { this.operatorProdavec = operatorProdavec; }

    public String getPovarUpakov() {
        return povarUpakov;
    }

    public void setPovarUpakov(String povarUpakov) {
        this.povarUpakov = povarUpakov;
    }

    public String getSamovivoz() {
        return samovivoz;
    }

    public void setSamovivoz(String samovivoz) {
        this.samovivoz = samovivoz;
    }

    public String getIdOrderState() {
        return idOrderState;
    }

    public void setIdOrderState(String idOrderState) {
        this.idOrderState = idOrderState;
    }

    public String getOnWork() {
        return onWork;
    }

    public void setOnWork(String onWork) {
        this.onWork = onWork;
    }

}
