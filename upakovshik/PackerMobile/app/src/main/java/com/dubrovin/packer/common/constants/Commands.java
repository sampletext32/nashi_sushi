package com.dubrovin.packer.common.constants;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class Commands {

    // enter in system
    public static final String commandLoginInSystem = "users.select.by_login_password";

    //return points of work
    public static final String commandGetPointWork = "points_work.select.all";

    //start work
    public static final String commandUsersUpdateStartWork = "users.update.start_work";

    //return orders
    public static final String commandSelectPackOrders = "orders.select.upakov_orders";

    //add new order to queue
    public static final String commandPackWorkInsertAddOrderToQueue = "upakovs_work.insert.add_order_to_queue";

    //finish work
    public static final String commandUsersUpdateFinishWork  = "users.update.finish_work";

    //start new order
    public static final String commandPackWorkUpdateOrderStart = "upakovs_work.update.order_start";

    //start work
    public static final String commandOrdersUpdateOnWorkStart= "orders.update.on_work_start";

    //finish working order
    public static final String commandPackWorkUpdateOrderFinish = "upakovs_work.update.order_finish";

    //add order to kurer
    public static final String commandOrdersUpdateMoveOrderToKurer = "orders.update.move_order_to_kurer";

    //finish order
    public static final String commandOrdersUpdateFinishOrder = "orders.update.finish_order";

    //return id products for order
    public static final String commandOrderProductsSelectByIdOrder = "order_products.select.by_id_order";

    //return list of products
    public static final String commandProductsSelectAll = "products.select.all";
}
