﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SushiLib
{
    [Serializable]
    public class Order
    {
        public int id;
        public int id_operator;
        public int id_povar;
        public int id_upakov;
        public int id_kurer;
        public int id_client;
        public int id_point_work;
        public int datetime_start;
        public int datetime_finish;
        public int duration;
        public int price;
        public int skidka;
        public int price_with_skidka;
        public int client_money;
        public int count_persons;
        public string description;//1000симв
        public string promocode;//20симв
        public int no_cash;
        public int operator_prodavec;
        public int povar_upakov;
        public int samovivoz;
        public int id_order_state;
        public int on_work;
        public int send_fiscal_data;
        public Order()
        {

        }
    }
}
