﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SushiLib
{
    [Serializable]
    public class Dolgnost
    {
        public int id;
        public string name;
        public override string ToString()
        {
            return id + ": " + name;
        }
        public Dolgnost()
        {

        }
    }
}
