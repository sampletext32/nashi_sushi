package com.dubrovin.courier.ui.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dubrovin.courier.R;
import com.dubrovin.courier.common.constants.Commands;
import com.dubrovin.courier.common.constants.Config;
import com.dubrovin.courier.common.constants.ErrorsInfo;
import com.dubrovin.courier.common.utils.GLog;
import com.dubrovin.courier.common.utils.SingletonFonts;
import com.dubrovin.courier.data.App;
import com.dubrovin.courier.data.models.AdvPoint;
import com.dubrovin.courier.data.models.City;
import com.dubrovin.courier.data.models.Client;
import com.dubrovin.courier.data.models.Order;
import com.dubrovin.courier.data.models.Product;
import com.dubrovin.courier.data.models.ProductId;
import com.dubrovin.courier.data.models.Rayon;
import com.dubrovin.courier.data.models.Status;
import com.dubrovin.courier.data.models.Street;
import com.dubrovin.courier.data.network.RetrofitSushi;
import com.dubrovin.courier.ui.adapters.ProductAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class WorkingOrderActivity extends AppCompatActivity{


    private TextView mTextViewClientPhoneNumber;
    private TextView mTextViewClientFirstName;
    private TextView mTextViewClientLastName;
    private TextView mTextViewClientCity;
    private TextView mTextViewClientRegion;
    private TextView mTextViewClientStreet;
    private TextView mTextViewClientHouse;
    private TextView mTextViewClientFlat;

    private TextView mTextViewClientOrderHouse;
    private TextView mTextViewClientCorpus;
    private TextView mTextViewClientMounth;

    private TextView mTextViewClientDay;
    private TextView mTextViewClientNumberCard;
    private TextView mTextViewClientAdds;

    private NotificationManager nm;


    private List<Product> mProducts = new ArrayList<>();

    private ProductAdapter mProductAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_working);

        GLog.i("onCreate WorkingOrderActivity");

        StartOrder();
        getIDsProducts();

        App app = (App)getApplicationContext();

        //region  notification
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (v != null) {
            v.vibrate(1500);
        }


        try
        {
            nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            nm.cancelAll();
        }
        catch (Exception e){e.printStackTrace();}

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
                nm.cancelAll();
            }
        }).start();
        //endregion


        mTextViewClientCity = findViewById(R.id.client_city);
        mTextViewClientPhoneNumber = findViewById(R.id.client_phone_number);
        mTextViewClientFirstName = findViewById(R.id.client_first_name);
        mTextViewClientLastName = findViewById(R.id.client_last_name);
        mTextViewClientRegion = findViewById(R.id.client_region);
        mTextViewClientStreet = findViewById(R.id.client_street);
        mTextViewClientHouse = findViewById(R.id.client_house);
        mTextViewClientFlat = findViewById(R.id.clent_flat);
        mTextViewClientOrderHouse = findViewById(R.id.client_other_house);
        mTextViewClientCorpus = findViewById(R.id.client_korpus);
        mTextViewClientMounth = findViewById(R.id.client_month);
        mTextViewClientDay = findViewById(R.id.client_day);
        mTextViewClientNumberCard = findViewById(R.id.client_number_card);
        mTextViewClientAdds = findViewById(R.id.client_adds);



        RecyclerView recyclerView = findViewById(R.id.recycler_view_working_activity_list_product);

        mProductAdapter = new ProductAdapter(mProducts);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mProductAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());




        Chronometer chronometer = findViewById(R.id.order_working_timer_chronometer);



        TextView textViewProducts = findViewById(R.id.order_working_products_text_view);



        TextView idOrder = findViewById(R.id.text_view_working_activity_id_order2);
        TextView otherOrder = findViewById(R.id.text_view_working_activity_other2);
        TextView priceOrder = findViewById(R.id.text_view_working_activity_price2);
        TextView countOrder = findViewById(R.id.text_view_working_activity_count_person2);

        idOrder.setText(app.getChooseOrder().getId());
        priceOrder.setText(app.getChooseOrder().getPrice());
        otherOrder.setText(app.getChooseOrder().getDescription());
        countOrder.setText(app.getChooseOrder().getCountPersons());


        // Chronometer
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        Button buttonExit = findViewById(R.id.button_working_activity_exit);

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final AlertDialog.Builder builder = new AlertDialog.Builder(WorkingOrderActivity.this);

                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.activity_write_distance,null);

                builder.setView(dialogView);

                final EditText editText = dialogView.findViewById(R.id.edit_text_write_distance);
                final Button buttonOk = dialogView.findViewById(R.id.button_ok_write_distance);
                final AVLoadingIndicatorView indicatorView = dialogView.findViewById(R.id.activity_write_distance_loader_spinner);

                builder.setCancelable(false);

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        loaderButtonCloseHide();
                    }
                });

                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loaderButtonCloseHide();
                        dialog.dismiss();
                    }
                });

                buttonOk.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String dist = editText.getText().toString();
                        GLog.i("click "+dist);
                        buttonOk.setVisibility(View.GONE);
                        indicatorView.setVisibility(View.VISIBLE);
                        WriteDistance(dist, new listner() {
                            @Override
                            public void error() {
                                buttonOk.setVisibility(View.VISIBLE);
                                indicatorView.setVisibility(View.GONE);
                                Toasty.error(getApplicationContext(),ErrorsInfo.errorNotKnowing,Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });


                builder.create().show();

            }
        });


        getClient();

    }

    private interface listner
    {
        void error();
    }


    private void WriteDistance(String dist, final listner listner)
    {

        JSONObject jsonObject = new JSONObject();

        App app = (App)getApplicationContext();


        try {
            jsonObject.put("id_order",app.getChooseOrder().getId());
            jsonObject.put("distance",dist);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RetrofitSushi.getApi().writeDistance(Commands.commandWriteDistance,jsonObject.toString(),Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                    FinishOrder(listner);
                else
                {
                    listner.error();
                    Toasty.error(getApplicationContext(),ErrorsInfo.errorNotKnowing,Toast.LENGTH_SHORT).show();
                    loaderButtonCloseHide();
                }

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                t.printStackTrace();
                listner.error();
                loaderButtonCloseHide();
            }
        });
    }

    private void getCity()
    {
        App app = (App)getApplicationContext();
        if(app.getCityList().isEmpty())
        {
            City city = new City();
            city.setName("не получен");
            app.setChooseCity(city);
        }
        else
        {
            boolean flag = true;
            for (City city:app.getCityList())
            {
                if(city.getId().equals(app.getChooseClient().getIdCity()))
                {
                    app.setChooseCity(city);
                    flag = false;
                }
            }
            if(flag) {
                City city = new City();
                city.setName("не получен");
                app.setChooseCity(city);
            }
        }

    }

    private void getRayon()
    {
        App app = (App)getApplicationContext();
        if(app.getRayonList().isEmpty())
        {
            Rayon rayon = new Rayon();
            rayon.setName("не получен");
            app.setChooseRayoun(rayon);
        }
        else {
            boolean flag = true;
            for (Rayon rayon : app.getRayonList()) {
                if (rayon.getId().equals(app.getChooseClient().getIdRayon())) {
                    app.setChooseRayoun(rayon);
                    flag = false;
                }
            }
            if(flag)
            {
                Rayon rayon = new Rayon();
                rayon.setName("не получен");
                app.setChooseRayoun(rayon);
            }
        }
    }

    private void getStreet()
    {
        App app = (App)getApplicationContext();
        if(app.getStreetList().isEmpty())
        {
            Street street = new Street();
            street.setName("не получен");
            app.setChooseStreet(street);
        }
        else
        {
            boolean flag = true;
            for (Street street:app.getStreetList())
            {
                if(street.getId().equals(app.getChooseClient().getIdStreet()))
                {
                    app.setChooseStreet(street);
                    flag = false;
                }
            }
            if(flag)
            {
                Street street = new Street();
                street.setName("не получен");
                app.setChooseStreet(street);
            }
        }

    }

    private void getAdvPoint()
    {
        App app = (App)getApplicationContext();
        if(app.getAdvPointList().isEmpty())
        {
            AdvPoint advPoint = new AdvPoint();
            advPoint.setName("не получен");
            app.setChooseAdvPoint(advPoint);
        }
        else {
            boolean flag = true;
            for (AdvPoint advPoint : app.getAdvPointList()) {
                if (advPoint.getId().equals(app.getChooseClient().getIdAdvpoint())) {
                    app.setChooseAdvPoint(advPoint);
                    flag = false;
                }
            }
            if(flag)
            {
                AdvPoint advPoint = new AdvPoint();
                advPoint.setName("не получен");
                app.setChooseAdvPoint(advPoint);
            }
        }
    }

    private void getClient()
    {
        final App app = (App) getApplicationContext();
        Toasty.info(this,"идет загрузка",Toast.LENGTH_SHORT).show();
        try {
            RetrofitSushi.getApi().getClient(Commands.commandGetClient, app.getChooseOrder().getIdClient(), Config.KEY).enqueue(new Callback<Client>() {
                @Override
                public void onResponse(Call<Client> call, Response<Client> response) {
                    if(response.isSuccessful())
                    {
                        app.setChooseClient(response.body());

                        getCity();
                        getStreet();
                        getAdvPoint();
                        getRayon();

                        mTextViewClientCity.setText(app.getChooseCity().getName());
                        mTextViewClientPhoneNumber.setText(app.getChooseClient().getNumberPhone());
                        mTextViewClientFirstName.setText(app.getChooseClient().getFirstName());
                        mTextViewClientLastName.setText(app.getChooseClient().getLastName());
                        mTextViewClientRegion.setText(app.getChooseRayoun().getName());
                        mTextViewClientStreet.setText(app.getChooseStreet().getName());
                        mTextViewClientHouse.setText(app.getChooseClient().getNumberHome());
                        mTextViewClientFlat.setText(app.getChooseClient().getNumberFlat());
                        mTextViewClientOrderHouse.setText(app.getChooseClient().getNumberBuild());
                        mTextViewClientCorpus.setText(app.getChooseClient().getNumberCorp());
                        mTextViewClientMounth.setText(app.getChooseClient().getBdMonth());
                        mTextViewClientDay.setText(app.getChooseClient().getBdDay());
                        mTextViewClientNumberCard.setText(app.getChooseClient().getNumberCard());
                        mTextViewClientAdds.setText(app.getChooseAdvPoint().getName());
                    }
                    else
                    {

                        finishActivity();
                        Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                    }

                }

                @Override
                public void onFailure(Call<Client> call, Throwable t) {
                    t.printStackTrace();
                    Toasty.error(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                    finishActivity();
                }
            });
        }
        catch (Exception e)
        {
            finishActivity();
            Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
        }
    }

    private void FinishOrder2(final listner listner){
        loaderButtonCloseShow();
        final App app = (App) getApplicationContext();
        RetrofitSushi.getApi().addLogUpdateOrderFinish(Commands.commandOrdersUpdateFinishOrder,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                {
                    loaderButtonCloseHide();
                    app.getOrdersList().clear();
                    Intent intent = new Intent(WorkingOrderActivity.this, CurrentOrdersActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                listner.error();
                t.printStackTrace();loaderButtonCloseHide();
            }
        });
    }

    private void FinishOrder(final listner listner)
    {
        loaderButtonCloseShow();
        App app = (App)getApplicationContext();
        RetrofitSushi.getApi().addLogUpdateOrderFinish(Commands.commandCourierWorkUpdateOrderFinish,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                {
                    loaderButtonCloseHide();
                    FinishOrder2(listner);
                }

            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                loaderButtonCloseHide();
                listner.error();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTextViewClientCity = null;
        mTextViewClientPhoneNumber = null;
        mTextViewClientFirstName = null;
        mTextViewClientLastName = null;
        mTextViewClientRegion = null;
        mTextViewClientStreet = null;
        mTextViewClientHouse = null;
        mTextViewClientFlat = null;
        mTextViewClientOrderHouse = null;
        mTextViewClientCorpus = null;
        mTextViewClientMounth = null;
        mTextViewClientDay = null;
        mTextViewClientNumberCard = null;
        mTextViewClientAdds = null;

    }

    private void StartOrder()
    {
        final App app = (App) getApplicationContext();

        try {
            RetrofitSushi.getApi().addLogUpdateOrderStart(Commands.commandCourierWorkUpdateOrderStart, app.getChooseOrder().getId(), Config.KEY, app.getChooseUser().getId()).enqueue(new Callback<Status>() {
                @Override
                public void onResponse(Call<Status> call, Response<Status> response) {
                    if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                    {
                        StartWorkOrder();
                    }
                    else
                    {
                        finishActivity();
                        Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                    }

                }

                @Override
                public void onFailure(Call<Status> call, Throwable t) {
                    t.printStackTrace();
                    Toasty.error(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                    finishActivity();
                }
            });
        }
        catch (Exception e)
        {
            finishActivity();
            Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
        }
    }

    private void StartWorkOrder()
    {
        App app = (App)getApplicationContext();

        RetrofitSushi.getApi().addLogUpdateOrderStartWork(Commands.commandOrdersUpdateOnWorkStart,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                {

                    Toasty.success(getApplicationContext(), "заказ начал выполняться", Toast.LENGTH_SHORT, true).show();
                }
                else
                {
                    finishActivity();
                    Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                }
            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                finishActivity();
                Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    private void finishActivity()
    {
        Intent intent = new Intent(WorkingOrderActivity.this, CurrentOrdersActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void loaderButtonCloseHide()
    {
//        mAVLoadingIndicatorView.hide();
//        mAVLoadingIndicatorView.setVisibility(View.GONE);
//        mSubmitProcessButtonCloseOrder.setVisibility(View.VISIBLE);
    }

    private void loaderButtonCloseShow()
    {
//        mAVLoadingIndicatorView.show();
//        mAVLoadingIndicatorView.setVisibility(View.VISIBLE);
//        mSubmitProcessButtonCloseOrder.setVisibility(View.GONE);
    }

    private void getIDsProducts()
    {

        final App app = (App)getApplicationContext();

        RetrofitSushi.getApi().getProductsIds(Commands.commandOrderProductsSelectByIdOrder,app.getChooseOrder().getId(), Config.KEY).enqueue(new Callback<List<ProductId>>() {

            @Override
            public void onResponse(Call<List<ProductId>> call, Response<List<ProductId>> response) {
                if(response.isSuccessful() && response.body().size()>0) {

                    List<Product> newProducts = new ArrayList<>();

                    app.getProdutsIds().clear();
                    app.getProdutsIds().addAll(response.body());


                    GLog.i("getIDsProducts    " + response.body().size());


                    for (ProductId id : app.getProdutsIds()) {
                        for (Product product : app.getProductList())
                            if (product.getId().equals(id.getId_product())) {
                                product.setCount(id.getCount());
                                newProducts.add(product);
                            }
                    }


                    mProducts.clear();
                    mProducts.addAll(newProducts);

                    mProductAdapter.update();

                }
                else
                {
                    getProducts();
                }

            }
            @Override
            public void onFailure(Call<List<ProductId>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void getProducts() {


        final App app = (App) getApplicationContext();

        RetrofitSushi.getApi().getProducts(Commands.commandProductsSelectAll, Config.KEY).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, final Response<List<Product>> response) {
                try {
                    if(response.isSuccessful())
                    {

                        GLog.i("getProducts    "+response.body().size());

                        app.getProductList().clear();
                        app.getProductList().addAll(response.body());
                        getIDsProducts();


                    }

                    GLog.i("Error get Products");
                }
                catch (NullPointerException e)
                {
                    Toasty.warning(getApplicationContext(), ErrorsInfo.errorProducts, Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorProducts, Toast.LENGTH_SHORT, true).show();
            }
        });
    }


    @Override
    public void onBackPressed() { }
}
