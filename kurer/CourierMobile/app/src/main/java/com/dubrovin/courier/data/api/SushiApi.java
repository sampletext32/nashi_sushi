package com.dubrovin.courier.data.api;

import com.dubrovin.courier.data.models.AdvPoint;
import com.dubrovin.courier.data.models.City;
import com.dubrovin.courier.data.models.Client;
import com.dubrovin.courier.data.models.Order;
import com.dubrovin.courier.data.models.PointWork;
import com.dubrovin.courier.data.models.Product;
import com.dubrovin.courier.data.models.ProductId;
import com.dubrovin.courier.data.models.Rayon;
import com.dubrovin.courier.data.models.Status;
import com.dubrovin.courier.data.models.Street;
import com.dubrovin.courier.data.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public interface SushiApi {
    @FormUrlEncoded
    @POST("/api.php/")
    Call<User> signIn(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<PointWork>> getPointsWork(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateStartWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<City>> getCity(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Street>> getStreet(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Rayon>> getRayon(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<AdvPoint>> getAdvPoint(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Order>> getAllOrders(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addOrderToQueue(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateFinishWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Client> getClient(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderStart(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderFinish(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderStartWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Product>> getProducts(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<ProductId>> getProductsIds(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateMoveOrder(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateFinishOrder(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> writeDistance(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);
}
