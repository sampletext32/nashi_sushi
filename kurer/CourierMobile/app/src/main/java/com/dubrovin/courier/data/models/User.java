package com.dubrovin.courier.data.models;

/**
 * Created by Dubrovin on 11.12.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    public User withId(String id) {
        this.id = id;
        return this;
    }

    public String getNumberPhone() {
        return numberPhone;
    }
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("number_phone")
    @Expose
    private String numberPhone;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("passport")
    @Expose
    private String passport;
    @SerializedName("id_dolgnost")
    @Expose
    private String idDolgnost;
    @SerializedName("id_point_work")
    @Expose
    private String idPointWork;
    @SerializedName("online")
    @Expose
    private String online;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public User withNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public User withLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User withPassword(String password) {
        this.password = password;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public User withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public User withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public User withPassport(String passport) {
        this.passport = passport;
        return this;
    }

    public String getIdDolgnost() {
        return idDolgnost;
    }

    public void setIdDolgnost(String idDolgnost) {
        this.idDolgnost = idDolgnost;
    }

    public User withIdDolgnost(String idDolgnost) {
        this.idDolgnost = idDolgnost;
        return this;
    }

    public String getIdPointWork() {
        return idPointWork;
    }

    public void setIdPointWork(String idPointWork) {
        this.idPointWork = idPointWork;
    }

    public User withIdPointWork(String idPointWork) {
        this.idPointWork = idPointWork;
        return this;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public User withOnline(String online) {
        this.online = online;
        return this;
    }
}