package com.dubrovin.courier.common.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class NetWorkUtil {
    private NetWorkUtil() {}

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static boolean validateUrl(String adress ){
        return android.util.Patterns.WEB_URL.matcher(adress).matches();
    }

}
