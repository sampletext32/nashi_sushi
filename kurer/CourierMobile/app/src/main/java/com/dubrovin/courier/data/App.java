package com.dubrovin.courier.data;

import android.app.Application;
import android.view.View;
import android.widget.Toast;

import com.dubrovin.courier.common.constants.Commands;
import com.dubrovin.courier.common.constants.Config;
import com.dubrovin.courier.common.constants.ErrorsInfo;
import com.dubrovin.courier.data.models.AdvPoint;
import com.dubrovin.courier.data.models.City;
import com.dubrovin.courier.data.models.Client;
import com.dubrovin.courier.data.models.Order;
import com.dubrovin.courier.data.models.PointWork;
import com.dubrovin.courier.data.models.Product;
import com.dubrovin.courier.data.models.ProductId;
import com.dubrovin.courier.data.models.Rayon;
import com.dubrovin.courier.data.models.Status;
import com.dubrovin.courier.data.models.Street;
import com.dubrovin.courier.data.models.User;
import com.dubrovin.courier.data.network.RetrofitSushi;
import com.squareup.leakcanary.LeakCanary;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by super on 18.02.2018.
 */

public class App extends Application {


    //Выбранные элементы
    private PointWork ChoosePointWork;
    private User ChooseUser;
    private Order ChooseOrder;
    private Client ChooseClient;
    private City ChooseCity;
    private Rayon ChooseRayoun;
    private Street ChooseStreet;
    private AdvPoint ChooseAdvPoint;

    //словари
    private List<Order> mOrdersList = new ArrayList<>();
    private List<AdvPoint> mAdvPointList = new ArrayList<>();
    private List<City> mCityList = new ArrayList<>();
    private List<Rayon> mRayonList = new ArrayList<>();
    private List<Street> mStreetList = new ArrayList<>();
    private List<PointWork> mPointWorks = new ArrayList<>();


    private List<Product> mProductList = new ArrayList<>();
    private List<ProductId> produtsIds = new ArrayList<>();



    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }


    public void AddOrderToQueue(String idUser,String idOrder,String dateTimeAdd,String dateTimeStart,String dateTimeFinish,String durationAddStart,String durationStartFinish)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id_user",idUser);
            jsonObject.put("id_order",idOrder);
            // jsonObject.put("datetime_add",dateTimeAdd);
            jsonObject.put("datetime_start",dateTimeStart);
            jsonObject.put("datetime_finish",dateTimeFinish);
            // jsonObject.put("duration_add_start",durationAddStart);
            jsonObject.put("duration_start_finish",durationStartFinish);
        } catch (JSONException e) {
            return;
        }

        RetrofitSushi.getApi().addOrderToQueue(Commands.commandCourierWorkInsertAddOrderToQueue,jsonObject.toString(),Config.KEY).enqueue(new Callback<Status>() {

            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                t.printStackTrace();
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    //region   get/set
    public List<Product> getProductList() {
        return mProductList;
    }

    public void setProductList(List<Product> productList) {
        mProductList = productList;
    }

    public List<ProductId> getProdutsIds() {
        return produtsIds;
    }

    public void setProdutsIds(List<ProductId> produtsIds) {
        this.produtsIds = produtsIds;
    }

    public Client getChooseClient() {
        return ChooseClient;
    }

    public void setChooseClient(Client chooseClient) {
        ChooseClient = chooseClient;
    }

    public City getChooseCity() {
        return ChooseCity;
    }

    public void setChooseCity(City chooseCity) {
        ChooseCity = chooseCity;
    }

    public Rayon getChooseRayoun() {
        return ChooseRayoun;
    }

    public void setChooseRayoun(Rayon chooseRayoun) {
        ChooseRayoun = chooseRayoun;
    }

    public Street getChooseStreet() {
        return ChooseStreet;
    }

    public void setChooseStreet(Street chooseStreet) {
        ChooseStreet = chooseStreet;
    }

    public AdvPoint getChooseAdvPoint() {
        return ChooseAdvPoint;
    }

    public void setChooseAdvPoint(AdvPoint chooseAdvPoint) {
        ChooseAdvPoint = chooseAdvPoint;
    }

    public List<PointWork> getPointWorks() {
        return mPointWorks;
    }

    public void setPointWorks(List<PointWork> pointWorks) {
        mPointWorks = pointWorks;
    }

    public PointWork getChoosePointWork() {
        return ChoosePointWork;
    }

    public void setChoosePointWork(PointWork choosePointWork) {
        ChoosePointWork = choosePointWork;
    }

    public User getChooseUser() {
        return ChooseUser;
    }

    public void setChooseUser(User chooseUser) {
        ChooseUser = chooseUser;
    }

    public Order getChooseOrder() {
        return ChooseOrder;
    }

    public void setChooseOrder(Order chooseOrder) {
        ChooseOrder = chooseOrder;
    }

    public List<Order> getOrdersList() {
        return mOrdersList;
    }

    public void setOrdersList(List<Order> ordersList) {
        mOrdersList = ordersList;
    }

    public List<AdvPoint> getAdvPointList() {
        return mAdvPointList;
    }

    public void setAdvPointList(List<AdvPoint> advPointList) {
        mAdvPointList = advPointList;
    }

    public List<City> getCityList() {
        return mCityList;
    }

    public void setCityList(List<City> cityList) {
        mCityList = cityList;
    }

    public List<Rayon> getRayonList() {
        return mRayonList;
    }

    public void setRayonList(List<Rayon> rayonList) {
        mRayonList = rayonList;
    }

    public List<Street> getStreetList() {
        return mStreetList;
    }

    public void setStreetList(List<Street> streetList) {
        mStreetList = streetList;
    }

    //endregion

}
